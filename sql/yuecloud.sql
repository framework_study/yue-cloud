create table sys_app_info
(
    id          bigint                 not null comment '唯一主键，appid'
        primary key,
    app_name    varchar(100)           not null comment '应用名称',
    app_secret  varchar(100)           not null comment '应用密钥，AES(appid+app_name)',
    public_key  varchar(1000)          not null comment '应用公钥',
    private_key varchar(1000)          not null comment '应用私钥',
    is_deleted  varchar(3) default '1' not null comment '0-否 1-是',
    create_time datetime               not null comment '创建时间',
    update_time datetime               not null comment '修改时间'
)
    comment '系统-应用信息表' collate = utf8mb4_bin;

create table sys_dict
(
    id        bigint       not null comment 'id'
        primary key,
    dict_code varchar(100) not null comment '字典码',
    dict_name varchar(200) not null comment '字典名称'
)
    comment '系统-字典表' collate = utf8mb4_bin;

create table sys_dict_item
(
    id         bigint       not null comment 'id'
        primary key,
    dict_id    bigint       not null comment '字典名称',
    dict_value varchar(100) not null comment '字典值',
    dict_name  varchar(200) not null comment '字典名称'
)
    comment '系统-字典值表' collate = utf8mb4_bin;

create table sys_task
(
    id              bigint       not null comment 'id'
        primary key,
    task_name       varchar(100) not null comment '定时任务名称',
    task_class_name varchar(200) not null comment '定时任务类名',
    task_cron       varchar(100) not null comment 'cron表达式',
    description     varchar(500) not null comment '说明',
    status          varchar(2)   null comment '01-草稿 02-待发布 03-待运行 04-运行中 05-已完成 06-执行异常',
    is_deleted      varchar(1)   not null comment '逻辑删除状态 0-删除 1-可用',
    create_time     datetime     not null comment '创建时间',
    update_time     datetime     not null comment '修改时间'
)
    comment '系统-定时任务表' collate = utf8mb4_bin;

create table sys_user
(
    id          bigint       not null
        primary key,
    username    varchar(100) null comment '用户名',
    password    varchar(100) null comment '密码',
    salt        varchar(100) null comment '盐',
    create_time datetime     null comment '创建时间',
    update_time datetime     null comment '修改时间',
    is_delete   char         null comment '0-删除 1-可用',
    status      char(2)      null comment '01-可用 02-禁用'
)
    comment '系统-用户表' collate = utf8mb4_bin;

