# 项目结构
```
|-- yue-cloud
    |-- yue-cloud-admin                        后台管理模块
    |   |-- yue-cloud-admin-system             
    |   |-- yue-cloud-admin-job                定时调度模块
    |   |-- yue-cloud-admin-auth               登录认证  
    |-- yue-cloud-common                       公共模块 
    |   |-- yue-cloud-common-discovery         nacos服务注册和发现  
    |   |-- yue-cloud-common-logger            日志模块
    |   |-- yue-cloud-common-db                持久层框架、动态数据源源配置
    |   |-- yue-cloud-common-object            实体、mapper文件
    |   |-- yue-cloud-common-redis             redis模块
    |   |-- yue-cloud-common-swagger           swagger模块
    |   |-- yue-cloud-common-tool              工具类
    |   |-- yue-cloud-common-web               web处理
    |-- yue-cloud-gateway                      网关模块
    |-- yue-cloud-service                      业务模块
        |-- yue-cloud-service-order
```

# 工程简介

# 延伸阅读

