package com.wuwy.common.web.asserts;

import com.wuwy.common.web.exception.YueException;

/**
 * @author :  wuwy
 * @date :  2023/4/24
 * @desc : 断言类
 */
public abstract class YueAssert {

    /**
     * 断言必须为null
     */
    public static void mustNull(Object obj, String message, Object... params) {
        mustTrue(obj == null, message, params);
    }

    /**
     * 断言必须不为null
     */
    public static void mustNotNull(Object obj, String message, Object... params) {
        mustTrue(obj != null, message, params);
    }

    /**
     * 断言必须为真
     */
    public static void mustTrue(boolean flag, String message, Object... params) {
        if (!flag) {
            throw new YueException(message, params);
        }
    }

    /**
     * 断言必须为假
     */
    public static void mustFalse(boolean flag, String message, Object... params) {
        if (flag) {
            throw new YueException(message, params);
        }
    }
}
