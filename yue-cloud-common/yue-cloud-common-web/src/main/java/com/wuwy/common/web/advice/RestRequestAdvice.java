package com.wuwy.common.web.advice;

import com.wuwy.common.object.entity.sys.SysAppInfoEntity;
import com.wuwy.common.object.mapper.sys.SysAppInfoMapper;
import com.wuwy.common.tool.utils.StreamUtil;
import com.wuwy.common.tool.utils.code.HexUtil;
import com.wuwy.common.tool.utils.constant.CypherConstants;
import com.wuwy.common.tool.utils.cypher.AESUtil;
import com.wuwy.common.tool.utils.cypher.CustomSignature;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import jakarta.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * @author :  wuwy
 * @date :  2022/10/17 15:54
 * @desc :
 */
@Slf4j
@ControllerAdvice
public class RestRequestAdvice extends RequestBodyAdviceAdapter {

    @Resource
    private SysAppInfoMapper sysAppInfoMapper;

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        RequestMethod[] method = methodParameter.getMethodAnnotation(RequestMapping.class).method();
        String methodName = method[0].name();
        // POST方法才进行加解密处理
        if (RequestMethod.POST.name().equals(methodName) && false) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter,
                                           Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        HttpHeaders headers = inputMessage.getHeaders();
        Map<String, String> headerMap = this.getHeaderString(headers);

        // 获取app密钥、公钥
        SysAppInfoEntity appInfo = sysAppInfoMapper.selectById(headerMap.get(CypherConstants.HEADER_APPID));
        if (appInfo == null) {
            throw new RuntimeException("appid不存在");
        }

        // 读取body字节流转换成string
        String encryptDataHex = StreamUtil.readText(inputMessage.getBody());
        log.info("接收到的密文: {}", encryptDataHex);

        try {
            // 数据解密
            byte[] decrypt = AESUtil.decrypt(HexUtil.hexStringToByte(encryptDataHex), AESUtil.getKey(appInfo.getAppSecret()));
            String res = new String(decrypt);
            log.info("解密: {}", res);

            // 验签
            headerMap.put("data", res);
            boolean verify = CustomSignature.verifyV2(headerMap, appInfo.getPublicKey(), CypherConstants.CHARSET_UTF8, CypherConstants.SIGN_TYPE_RSA);

            if (!verify) {
                // 签名不对
                throw new RuntimeException("签名校验失败");
            }

            final ByteArrayInputStream bais = new ByteArrayInputStream(decrypt);
            return new HttpInputMessage() {
                @Override
                public InputStream getBody() throws IOException {
                    return bais;
                }

                @Override
                public HttpHeaders getHeaders() {
                    return inputMessage.getHeaders();
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.beforeBodyRead(inputMessage, parameter, targetType, converterType);
    }

    private Map<String, String> getHeaderString(HttpHeaders headers) {
        Map<String, String> map = new HashMap();
        map.put(CypherConstants.HEADER_SIGN, headers.get(CypherConstants.HEADER_SIGN).get(0));
        map.put(CypherConstants.HEADER_TIMESTAMP, headers.get(CypherConstants.HEADER_TIMESTAMP).get(0));
        map.put(CypherConstants.HEADER_APPID, headers.get(CypherConstants.HEADER_APPID).get(0));
        map.put(CypherConstants.HEADER_NONCESTR, headers.get(CypherConstants.HEADER_NONCESTR).get(0));
        return map;
    }
}