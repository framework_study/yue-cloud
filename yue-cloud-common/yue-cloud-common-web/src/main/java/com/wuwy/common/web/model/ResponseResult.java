package com.wuwy.common.web.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * @author :  wuwy
 * @date :  2022/10/17 15:46
 * @desc : 接口返回结果类
 */
@Data
@Schema(name = "接口返回结果类")
public class ResponseResult<T> implements Serializable {

    private static final long serialVersionUID = -887502874744945344L;

    @Schema(description = "返回结果代码")
    private int code;

    @Schema(description = "返回提示")
    private String message;

    @Schema(description = "返回值")
    private T result;

    public ResponseResult() {
    }

    public ResponseResult(int code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public static ResponseResult success(String message, Object result) {
        return new ResponseResult(200, message, result);
    }

    public static ResponseResult success(Object result) {
        return success("成功", result);
    }


    public static ResponseResult error(String message) {
        return error(message, null);
    }

    public static ResponseResult error(String message, Object result) {
        return error(500, message, result);
    }

    public static ResponseResult error(int code, String message, Object result) {
        return new ResponseResult(code, message, result);
    }

}
