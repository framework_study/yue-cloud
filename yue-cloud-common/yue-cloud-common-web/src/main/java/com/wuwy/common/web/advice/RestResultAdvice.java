package com.wuwy.common.web.advice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.wuwy.common.object.entity.sys.SysAppInfoEntity;
import com.wuwy.common.object.mapper.sys.SysAppInfoMapper;
import com.wuwy.common.tool.utils.CollectionUtil;
import com.wuwy.common.tool.utils.StringUtil;
import com.wuwy.common.tool.utils.code.HexUtil;
import com.wuwy.common.tool.utils.constant.CypherConstants;
import com.wuwy.common.tool.utils.cypher.AESUtil;
import com.wuwy.common.web.model.ResponseResult;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import jakarta.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author :  wuwy
 * @date :  2022/10/17 15:54
 * @desc :
 */
@ControllerAdvice(basePackages = {"com.wuwy"})
public class RestResultAdvice implements ResponseBodyAdvice<Object> {

    @Resource
    private SysAppInfoMapper sysAppInfoMapper;

    @Override
    public boolean supports(MethodParameter returnType,
                            Class<? extends HttpMessageConverter<?>> converterType) {

        // true 转换 false 不转换
        Class<?> returnType1 = returnType.getMethod().getReturnType();
        String returnTypeName = returnType1.getName();
        // return !returnType1.isPrimitive() &&
        //         !returnTypeName.equals(String.class.getName()) &&
        //         !returnTypeName.equals(ResponseResult.class.getName());
        return !returnTypeName.equals(ResponseResult.class.getName());
    }

    @Override
    public Object beforeBodyWrite(Object o,
                                  MethodParameter returnType,
                                  MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request,
                                  ServerHttpResponse response) {
        List<String> appid = request.getHeaders().get(CypherConstants.HEADER_APPID);
        if (!CollectionUtil.isEmpty(appid) && StringUtil.isNotEmpty(appid.get(0))) {
            // 获取app密钥、公钥
            SysAppInfoEntity appInfo = sysAppInfoMapper.selectById(appid.get(0));
            if (appInfo == null) {
                throw new RuntimeException("appid不存在");
            } else {
                // 如果直接响应字符串返回,则会报类型转换异常.
                if (o instanceof ResponseResult) {
                    // 如果已经是ResultVO类型， 就没必要再包装一层了
                    // ResponseResult.result需要做加密处理
                    ResponseResult res = (ResponseResult) o;
                    byte[] encrypt = new byte[0];
                    try {
                        encrypt = AESUtil.encrypt(JSON.toJSONString(res.getResult()).getBytes(StandardCharsets.UTF_8), AESUtil.getKey(appInfo.getAppSecret()));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    res.setResult(HexUtil.bytesToHexString(encrypt));
                    return res;
                } else {
                    // ResponseResult.result需要做加密处理
                    String res = JSON.toJSONString(o);
                    byte[] encrypt = new byte[0];
                    try {
                        encrypt = AESUtil.encrypt(res.getBytes(), AESUtil.getKey(appInfo.getAppSecret()));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    return JSONObject.toJSONString(ResponseResult.success(HexUtil.bytesToHexString(encrypt)));
                }
            }
        } else {
            // 如果直接响应字符串返回,则会报类型转换异常.
            if (o instanceof String) {
                return JSONObject.toJSONString(ResponseResult.success(o));
            }
            // 如果已经是ResultVO类型， 就没必要再包装一层了
            if (o instanceof ResponseResult) {
                return o;
            }
            return new ResponseResult(200, "成功", o);
        }
    }
}