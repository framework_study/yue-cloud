package com.wuwy.common.web.exception;

import java.text.MessageFormat;

/**
 * @author :  wuwy
 * @date :  2022/11/18 11:28
 * @desc : 定义全局异常类 继承RuntimeException类
 */
public class YueException extends RuntimeException {

    public YueException() {
        super();
    }

    public YueException(String message) {
        super(message);
    }

    public YueException(String message, Throwable cause) {
        super(message, cause);
    }

    public YueException(Throwable cause) {
        super(cause);
    }

    public YueException(String message, Object... params) {
        this(MessageFormat.format(message, params));
    }

    public YueException(String message, Throwable cause, Object... params) {
        this(MessageFormat.format(message, params), cause);
    }
}
