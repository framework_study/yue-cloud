package com.wuwy.common.web.api;

import com.wuwy.common.object.entity.sys.SysAppInfoEntity;
import com.wuwy.common.object.mapper.sys.SysAppInfoMapper;
import com.wuwy.common.tool.utils.code.HexUtil;
import com.wuwy.common.tool.utils.constant.CypherConstants;
import com.wuwy.common.tool.utils.cypher.AESUtil;
import com.wuwy.common.tool.utils.cypher.CustomSignature;
import lombok.extern.slf4j.Slf4j;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author :  wuwy
 * @date :  2022/11/4 16:14
 * @desc :
 */
@Slf4j
//@Api("签名加密测试")
//@RestController
//@RequestMapping("/cypher")
public class CypherController {

    @Resource
    private SysAppInfoMapper sysAppInfoMapper;

//    @GetMapping("/getSign")
//    @ApiOperation(value = "获取签名", notes = "获取签名")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "body", value = "数据json", required = true, dataType = "string")
//    })
    public Map<String, String> getSign(String body, HttpServletRequest request) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put(CypherConstants.HEADER_TIMESTAMP, request.getHeader(CypherConstants.HEADER_TIMESTAMP));
        map.put(CypherConstants.HEADER_APPID, request.getHeader(CypherConstants.HEADER_APPID));
        map.put(CypherConstants.HEADER_NONCESTR, request.getHeader(CypherConstants.HEADER_NONCESTR));
        SysAppInfoEntity appInfo = sysAppInfoMapper.selectById(request.getHeader(CypherConstants.HEADER_APPID));

        if (appInfo != null) {
            map.put("data", body);
            // 加密
            byte[] encrypt = AESUtil.encrypt(body.getBytes(StandardCharsets.UTF_8), AESUtil.getKey(appInfo.getAppSecret()));
            // 加签
            String sign = CustomSignature.sign(map, appInfo.getPrivateKey(), CypherConstants.CHARSET_UTF8, CypherConstants.SIGN_TYPE_RSA);

            map.put(CypherConstants.HEADER_SIGN, sign);
            map.put("data", HexUtil.bytesToHexString(encrypt));

        }
        return map;
    }


}
