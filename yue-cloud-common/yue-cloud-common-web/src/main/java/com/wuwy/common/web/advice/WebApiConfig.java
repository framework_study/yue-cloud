package com.wuwy.common.web.advice;

import jakarta.annotation.Resource;
import jakarta.servlet.MultipartConfigElement;
import jakarta.servlet.Servlet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;


/**
 * @author :  wuwy
 * @date :  2022/11/15 11:15
 * @desc : 设置url前缀
 */
@Configuration
public class WebApiConfig {

    @Resource
    private MultipartConfigElement multipartConfigElement;

    @Bean
    @ConditionalOnMissingBean(name = "dispatcherRegistration")
    public ServletRegistrationBean<?> dispatcherRegistration(DispatcherServlet dispatcherServlet) {
        ServletRegistrationBean<?> registrationBean = new ServletRegistrationBean<Servlet>(dispatcherServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/api/*");
        registrationBean.setMultipartConfig(multipartConfigElement);
        registrationBean.setName("/api");
        return registrationBean;
    }
}
