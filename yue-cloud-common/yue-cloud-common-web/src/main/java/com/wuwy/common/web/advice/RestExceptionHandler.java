package com.wuwy.common.web.advice;

import com.wuwy.common.web.exception.YueBindException;
import com.wuwy.common.web.model.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * @author :  wuwy
 * @date :  2022/11/21 14:40
 * @desc : 统一异常格式处理
 */
@Slf4j
@RestControllerAdvice
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class RestExceptionHandler {

    // 明确要处理的异常类型，与此对应的异常类型会执行此方法进行处理
    @ExceptionHandler(RuntimeException.class)
    public ResponseResult<String> exceptionOther(Exception e) {
        // 出现异常后，服务器自身进行捕获，方便后期调试
        log.error("错误: ");
        this.printLog(e);
        // 返回状态码以及错误信息
        return ResponseResult.error("错误: " + e.getMessage());
    }

    // 明确要处理的异常类型，与此对应的异常类型会执行此方法进行处理
    @ExceptionHandler({YueBindException.class, BindException.class})
    public ResponseResult<String> exceptionBind(Exception e) {
        // 返回第一个参数错误
        BindingResult bindingResult = (BindingResult) e;
        FieldError fieldError = bindingResult.getFieldErrors().get(0);
        String errorMsg = fieldError.getDefaultMessage();
        String fieldInfo = fieldError.getField();


        // 返回状态码以及错误信息
        return ResponseResult.error("参数绑定错误: " + fieldInfo + ": " + errorMsg + "!");
    }

    public synchronized void printLog(Exception exception) {
        // 换行符
        String lineSeparatorStr = System.getProperty("line.separator");

        StringBuffer exStr = new StringBuffer();
        StackTraceElement[] trace = exception.getStackTrace();
        // 获取堆栈信息并输出为打印的形式
        for (StackTraceElement s : trace) {
            exStr.append("\tat " + s + "\r\n");
        }
        log.error("================================================================>");
        // 打印error级别的堆栈日志
        log.error("错误堆栈信息如下:" + exception.toString() + lineSeparatorStr + exStr);
        log.error("================================================================>");
    }
}

