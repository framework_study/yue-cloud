package com.wuwy.common.task.config;

import org.quartz.Scheduler;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import jakarta.annotation.Resource;
import javax.sql.DataSource;
import java.util.concurrent.Executor;

/**
 * @author :  wuwy
 * @date :  2022/11/17 9:33
 * @desc : 注册调度工厂
 */
@Configuration
public class ScheduleJobConfig {

    @Resource(name = "taskExecutor")
    private Executor taskExecutor;

    @Resource
    private DataSource dataSource;

    @Bean("schedulerFactoryBean")
    public SchedulerFactoryBean createFactoryBean(JobFactory jobFactory) {
        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
        factoryBean.setJobFactory(jobFactory);
        factoryBean.setDataSource(dataSource);
        factoryBean.setTaskExecutor(taskExecutor);
        factoryBean.setOverwriteExistingJobs(true);
        return factoryBean;
    }

    // 通过这个类对定时任务进行操作
    @Bean
    public Scheduler scheduler(@Qualifier("schedulerFactoryBean") SchedulerFactoryBean factoryBean) {
        return factoryBean.getScheduler();
    }
}