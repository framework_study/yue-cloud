package com.wuwy.common.tool.utils;

/**
 * @author :  wuwy
 * @date :  2022/10/26 13:10
 * @desc :
 */
public class SignSourceData {

    /**
     * 待验签原始内容
     */
    private final String sourceData;
    /**
     * 待验签原始内容在响应字符串中的起始位置
     */
    private final int    beginIndex;
    /**
     * 待验签原始内容在响应字符串中的结束位置
     */
    private final int    endIndex;

    public SignSourceData(String sourceData, int beginIndex, int endIndex) {
        this.sourceData = sourceData;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    public String getSourceData() {
        return sourceData;
    }

    public int getBeginIndex() {
        return beginIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }
}
