package com.wuwy.common.tool.utils.constant;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : 系统变量
 */
public class YueConstants {

    public final static Integer YUE_FALSE = 0;
    public final static Integer YUE_TRUE = 1;
}
