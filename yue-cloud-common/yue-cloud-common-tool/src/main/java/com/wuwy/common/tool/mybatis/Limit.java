package com.wuwy.common.tool.mybatis;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : 分页
 */
@Data
public class Limit {

    /**
     * 当前页
     */
    private Long current;

    /**
     * 每页显示条数，默认 10
     */
    private Long size;

    /**
     * 正序字段
     */
    private String asc = null;

    /**
     * 倒序字段
     */
    private String desc = null;

    public IPage page() {
        return new Page(PageParams.defaultCurrent(this.current), PageParams.defaultSize(this.size));
    }
}
