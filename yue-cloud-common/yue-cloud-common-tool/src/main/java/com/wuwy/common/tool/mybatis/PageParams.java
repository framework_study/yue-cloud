package com.wuwy.common.tool.mybatis;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : 分页参数
 */
public class PageParams {
    /**
     * 当前页
     */
    private static final long CURRENT = 1;

    /**
     * 每页显示条数，默认 10
     */
    private static final long SIZE = 10;

    public static long defaultCurrent(Long current) {
        return current == null || current.longValue() <= 0l ? PageParams.CURRENT : current;
    }

    public static long defaultSize(Long size) {
        return size == null || size.longValue() <= 0l ? PageParams.SIZE : size;
    }

}
