package com.wuwy.common.tool.utils;

import com.wuwy.common.tool.utils.constant.CypherConstants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * @author :  wuwy
 * @date :  2022/10/26 11:27
 * @desc :
 */
public class StrngHashMap extends HashMap<String, String> {

    private static final long serialVersionUID = -1277791390393392630L;

    public StrngHashMap() {
        super();
    }

    public StrngHashMap(Map<? extends String, ? extends String> m) {
        super(m);
    }

    public String put(String key, Object value) {
        String strValue;

        if (value == null) {
            strValue = null;
        } else if (value instanceof String) {
            strValue = (String) value;
        } else if (value instanceof Integer) {
            strValue = ((Integer) value).toString();
        } else if (value instanceof Long) {
            strValue = ((Long) value).toString();
        } else if (value instanceof Float) {
            strValue = ((Float) value).toString();
        } else if (value instanceof Double) {
            strValue = ((Double) value).toString();
        } else if (value instanceof Boolean) {
            strValue = ((Boolean) value).toString();
        } else if (value instanceof Date) {
            DateFormat format = new SimpleDateFormat(CypherConstants.DATE_TIME_FORMAT);
            format.setTimeZone(TimeZone.getTimeZone(CypherConstants.DATE_TIMEZONE));
            strValue = format.format((Date) value);
        } else {
            strValue = value.toString();
        }

        return this.put(key, strValue);
    }

    public String put(String key, String value) {
        if (StringUtil.areNotEmpty(key, value)) {
            return super.put(key, value);
        } else {
            return null;
        }
    }

}