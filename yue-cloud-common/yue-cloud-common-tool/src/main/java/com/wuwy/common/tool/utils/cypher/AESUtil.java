package com.wuwy.common.tool.utils.cypher;

import com.alibaba.fastjson.JSON;
import com.wuwy.common.tool.utils.code.HexUtil;
import com.wuwy.common.tool.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.SecureRandom;

/**
 * @author :  wuwy
 * @date :  2022/10/26 9:51
 * @desc : AESUtil
 */
public class AESUtil {
    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

    /**
     * 指定随机字符串（密码）生成密钥
     *
     * @param randomKey 加解密的密码
     * @throws Exception
     */
    public static byte[] getSecretKey(String randomKey) throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM); //秘钥生成器，指定秘钥算法

        //初始化此密钥生成器，指定AES的秘钥长度为128
        if (StringUtil.isBlank(randomKey)) {   //不指定密码
            keyGenerator.init(128);
        } else {    //指定密码
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(randomKey.getBytes());
            keyGenerator.init(128, random);
        }

        SecretKey secretKey = keyGenerator.generateKey();   //生成密钥
        return secretKey.getEncoded();
    }

    /**
     * 加密
     *
     * @param data 待加密数据
     * @param key  密钥
     * @return byte[]   加密数据
     * @throws Exception
     */
    public static byte[] encrypt(byte[] data, Key key) throws Exception {
        return encrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
    }

    /**
     * 加密
     *
     * @param data 待加密数据
     * @param key  二进制密钥
     * @return byte[]   加密数据
     * @throws Exception
     */
    public static byte[] encrypt(byte[] data, byte[] key) throws Exception {
        return encrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
    }

    /**
     * 加密
     *
     * @param data            待加密数据
     * @param key             二进制密钥
     * @param cipherAlgorithm 加密算法/工作模式/填充方式
     * @return byte[]   加密数据
     * @throws Exception
     */
    public static byte[] encrypt(byte[] data, byte[] key, String cipherAlgorithm) throws Exception {
        Key k = toKey(key);
        return encrypt(data, k, cipherAlgorithm);
    }

    /**
     * 加密
     *
     * @param data            待加密数据
     * @param key             密钥
     * @param cipherAlgorithm 加密算法/工作模式/填充方式
     * @return byte[]   加密数据
     * @throws Exception
     */
    public static byte[] encrypt(byte[] data, Key key, String cipherAlgorithm) throws Exception {
        Cipher cipher = Cipher.getInstance(cipherAlgorithm);    //获取算法
        cipher.init(Cipher.ENCRYPT_MODE, key);                  //设置加密模式，并指定秘钥
        return cipher.doFinal(data);                            //加密数据
    }

    /**
     * 解密
     *
     * @param data 待解密数据
     * @param key  二进制密钥
     * @return byte[]   解密数据
     * @throws Exception
     */
    public static byte[] decrypt(byte[] data, byte[] key) throws Exception {
        return decrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
    }

    /**
     * 解密
     *
     * @param data 待解密数据
     * @param key  密钥
     * @return byte[]   解密数据
     * @throws Exception
     */
    public static byte[] decrypt(byte[] data, Key key) throws Exception {
        return decrypt(data, key, DEFAULT_CIPHER_ALGORITHM);
    }

    /**
     * 解密
     *
     * @param data            待解密数据
     * @param key             二进制密钥
     * @param cipherAlgorithm 加密算法/工作模式/填充方式
     * @return byte[]   解密数据
     * @throws Exception
     */
    public static byte[] decrypt(byte[] data, byte[] key, String cipherAlgorithm) throws Exception {
        Key k = toKey(key);
        return decrypt(data, k, cipherAlgorithm);
    }

    /**
     * 解密
     *
     * @param data            待解密数据
     * @param key             密钥
     * @param cipherAlgorithm 加密算法/工作模式/填充方式
     * @return byte[]   解密数据
     * @throws Exception
     */
    public static byte[] decrypt(byte[] data, Key key, String cipherAlgorithm) throws Exception {
        Cipher cipher = Cipher.getInstance(cipherAlgorithm);     //获取算法
        cipher.init(Cipher.DECRYPT_MODE, key);                   //设置解密模式，并指定秘钥
        return cipher.doFinal(data);                             //解密数据
    }

    /**
     * 转换密钥
     *
     * @param secretKey 二进制密钥
     * @return 密钥
     */
    public static Key toKey(byte[] secretKey) {
        return new SecretKeySpec(secretKey, KEY_ALGORITHM);   //生成密钥
    }

    /**
     * 生成app_secket
     * @param  :
     * @return :
     */
    public static String generateAppSecket(String str) {
        // 生成app_secket
        byte[] secretKey = new byte[0];
        try {
            secretKey = getSecretKey(str);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Key key = toKey(secretKey);
        return HexUtil.bytesToHexString(key.getEncoded());
    }


    /**
     * 获取Key
     * @param  :
     * @return :
     */
    public static Key getKey(String secret) {
        byte[] secretKey = new byte[0];
        try {
            secretKey = getSecretKey(secret);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Key key = toKey(secretKey);
        return key;
    }

    public static void main(String[] args) throws Exception {
        String password = "EDD0E1DBD4853C6A3450B15A40E6D88C";


        Key key = getKey(password);

        System.out.println(password);

        String data = "{\"reqBody\": \"AES 对称加密算法\",\"key\":\"aes\"}";
        System.out.println("明文 ：" + data);

        byte[] encryptData = encrypt(data.getBytes(), key);
        String encryptDataHex = HexUtil.bytesToHexString(encryptData);   //把密文转为16进制
        System.out.println("加密 : " + encryptDataHex);

        byte[] decryptData = decrypt(encryptData, key);
        System.out.println("解密 : " + new String(decryptData));

    }
}
