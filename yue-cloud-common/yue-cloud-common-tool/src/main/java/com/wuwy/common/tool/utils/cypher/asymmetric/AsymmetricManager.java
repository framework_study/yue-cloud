package com.wuwy.common.tool.utils.cypher.asymmetric;

import com.wuwy.common.tool.utils.constant.CypherConstants;

/**
 * @author :  wuwy
 * @date :  2022/10/26 11:00
 * @desc : 非对称加密算法管理类
 */
public class AsymmetricManager {
    public static IAsymmetricEncryptor getByName(String type) {
        if (CypherConstants.SIGN_TYPE_RSA.equals(type)) {
            return new RSAEncryptor();
        }
        if (CypherConstants.SIGN_TYPE_RSA2.equals(type)) {
            return new RSA2Encryptor();
        }
        if (CypherConstants.SIGN_TYPE_SM2.equals(type)) {
            return new SM2Encryptor();
        }
        throw new RuntimeException("无效的非对称加密类型:[\" + type + \"]，可选值为：RSA、RSA2和SM2。");
    }
}
