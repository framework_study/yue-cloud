package com.wuwy.common.tool.utils;

/**
 * @author :  wuwy
 * @date :  2022/10/26 13:07
 * @desc :
 */
public class RequestParametersHolder {
    private StrngHashMap protocalMustParams;
    private StrngHashMap protocalOptParams;
    private StrngHashMap applicationParams;

    public StrngHashMap getProtocalMustParams() {
        return protocalMustParams;
    }

    public void setProtocalMustParams(StrngHashMap protocalMustParams) {
        this.protocalMustParams = protocalMustParams;
    }

    public StrngHashMap getProtocalOptParams() {
        return protocalOptParams;
    }

    public void setProtocalOptParams(StrngHashMap protocalOptParams) {
        this.protocalOptParams = protocalOptParams;
    }

    public StrngHashMap getApplicationParams() {
        return applicationParams;
    }

    public void setApplicationParams(StrngHashMap applicationParams) {
        this.applicationParams = applicationParams;
    }
}

