package com.wuwy.common.tool.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ClassUtils;
import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author :  wuwy
 * @date :  2022/8/25 15:05
 * @desc :
 */
public class BeanUtil extends org.springframework.beans.BeanUtils {

    static {
        ConvertUtils.register(new DateConverter(null), java.util.Date.class);
        ConvertUtils.register(new DateConverter(null), java.sql.Date.class);
    }

    public static <F, T> IPage<T> copy(IPage<F> fromPage, Class<T> tClass) {
        if (fromPage == null || fromPage.getRecords() == null || fromPage.getRecords().isEmpty()) {
            return (IPage<T>) fromPage;
        }
        List<T> tList = copy(fromPage.getRecords(), tClass);

        IPage<T> newPage = ClassUtils.newInstance(fromPage.getClass());

        newPage.setCurrent(fromPage.getCurrent());
        newPage.setPages(fromPage.getPages());
        newPage.setSize(fromPage.getSize());
        newPage.setTotal(fromPage.getTotal());
        newPage.setRecords(tList);

        return newPage;
    }

    public static <F, T> List<T> copy(List<F> fromList, Class<T> tClass) {
        if (fromList == null) {
            return null;
        }
        List<T> tList = new ArrayList<>();

        for (F f : fromList) {
            T t = copy(f, tClass);
            tList.add(t);
        }
        return tList;
    }

    public static <F, T> T copy(F entity, Class<T> modelClass) {

        Object model = null;

        if (entity == null || modelClass == null) {
            return null;
        }

        boolean entityIsMap = (entity instanceof Map);

        boolean modelIsMap = (Map.class.isAssignableFrom(modelClass));

        if (entityIsMap && modelIsMap) {
            model = BeanUtils.instantiateClass(modelClass);
            ((Map) model).putAll((Map) entity);
            return (T) model;
        } else if (entityIsMap && !modelIsMap) {
            try {
                model = BeanUtils.instantiateClass(modelClass);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            try {
                org.apache.commons.beanutils.BeanUtils.populate(model, (Map<String, ? extends Object>) entity);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if (!entityIsMap && modelIsMap) {
            model = new BeanMap(entity);
        } else {
            try {
                model = BeanUtils.instantiateClass(modelClass);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            BeanUtils.copyProperties(entity, model);
        }

        return (T) model;
    }

}
