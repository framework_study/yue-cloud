package com.wuwy.common.tool.utils.cypher;

import com.wuwy.common.tool.utils.*;
import com.wuwy.common.tool.utils.constant.CypherConstants;
import com.wuwy.common.tool.utils.cypher.asymmetric.AsymmetricManager;
import com.wuwy.common.tool.utils.cypher.asymmetric.RSAEncryptor;

import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;

/**
 * @author :  wuwy
 * @date :  2022/10/26 10:57
 * @desc : 签名工具类
 */
public class CustomSignature {
    /**
     * 通用签名方法
     *
     * @param content    待签名内容
     * @param privateKey 私钥
     * @param charset    编码格式
     * @param signType   签名类型：RSA、RSA2、SM2
     * @return
     * @throws 
     */
    public static String sign(String content, String privateKey, String charset,
                              String signType) throws RuntimeException {

        return AsymmetricManager.getByName(signType).sign(content, charset, privateKey);
    }

    /**
     * 通用签名方法
     *
     * @param params     待签名内容
     * @param privateKey 私钥
     * @param charset    编码格式
     * @param signType   签名类型：RSA、RSA2、SM2
     * @return
     * @throws RuntimeException
     */
    public static String sign(Map<String, String> params, String privateKey, String charset,
                              String signType) throws RuntimeException {
        String signContent = getSignContent(params);
        return AsymmetricManager.getByName(signType).sign(signContent, charset, privateKey);
    }

    /**
     * 密钥模式RSA、RSA2、SM2通用验签方法，主要用于支付接口的返回参数的验签比如：当面付，APP支付，手机网站支付，电脑网站支付
     *
     * @param params    待验签的从支付宝接收到的参数Map
     * @param publicKey 支付宝公钥
     * @param charset   参数内容编码集
     * @param signType  指定采用的签名方式，RSA、RSA2、SM2
     * @return true：验签通过；false：验签不通过
     * @throws RuntimeException
     */
    public static boolean verifyV1(Map<String, String> params, String publicKey,
                                   String charset, String signType) throws RuntimeException {
        String sign = params.get("sign");
        String content = getSignCheckContentV1(params);

        return AsymmetricManager.getByName(signType).verify(content, charset, publicKey, sign);
    }

    /**
     * 证书模式RSA、RSA2、SM2通用验签方法，主要用于支付接口的返回参数的验签比如：当面付，APP支付，手机网站支付，电脑网站支付
     *
     * @param params               待验签的从支付宝接收到的参数Map
     * @param alipayPublicCertPath 支付宝公钥路径
     * @param charset              参数内容编码集
     * @param signType             指定采用的签名方式，RSA、RSA2、SM2
     * @return true：验签通过；false：验签不通过
     * @throws RuntimeException
     */
    public static boolean certVerifyV1(Map<String, String> params, String alipayPublicCertPath,
                                       String charset, String signType) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);
        return verifyV1(params, publicKey, charset, signType);
    }

    /**
     * 密钥模式RSA、RSA2、SM2通用验签方法，主要是用于生活号相关的事件消息和口碑服务市场订购信息等发送到应用网关地址的异步信息的验签
     *
     * @param params    待验签的从支付宝接收到的参数Map
     * @param publicKey 支付宝公钥
     * @param charset   参数内容编码集
     * @param signType  指定采用的签名方式，RSA、RSA2、SM2
     * @return true：验签通过；false：验签不通过
     * @throws RuntimeException
     */
    public static boolean verifyV2(Map<String, String> params, String publicKey,
                                   String charset, String signType) throws RuntimeException {
        String sign = params.get("sign");
        String content = getSignCheckContentV2(params);

        return AsymmetricManager.getByName(signType).verify(content, charset, publicKey, sign);
    }

    /**
     * 证书模式RSA、RSA2、SM2通用验签方法，主要是用于生活号相关的事件消息和口碑服务市场订购信息等发送到应用网关地址的异步信息的验签
     *
     * @param params               待验签的从支付宝接收到的参数Map
     * @param alipayPublicCertPath 支付宝公钥路径
     * @param charset              参数内容编码集
     * @param signType             指定采用的签名方式，RSA、RSA2、SM2
     * @return true：验签通过；false：验签不通过
     * @throws RuntimeException
     */
    public static boolean certVerifyV2(Map<String, String> params, String alipayPublicCertPath,
                                       String charset, String signType) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);

        return verifyV2(params, publicKey, charset, signType);
    }

    /**
     * 密钥模式RSA、RSA2、SM2通用验签方法
     *
     * @param content   待验签字符串
     * @param sign      签名
     * @param publicKey 支付宝公钥
     * @param charset   参数内容编码集
     * @param signType  指定采用的签名方式，RSA、RSA2、SM2
     * @return
     * @throws RuntimeException
     */
    public static boolean verify(String content, String sign, String publicKey, String charset,
                                 String signType) throws RuntimeException {

        return AsymmetricManager.getByName(signType).verify(content, charset, publicKey, sign);
    }

    /**
     * 证书模式RSA、RSA2、SM2通用验签方法
     *
     * @param content              待验签字符串
     * @param sign                 签名
     * @param alipayPublicCertPath 支付宝公钥路径
     * @param charset              参数内容编码集
     * @param signType             指定采用的签名方式，RSA、RSA2、SM2
     * @return
     * @throws RuntimeException
     */
    public static boolean certVerify(String content, String sign, String alipayPublicCertPath, String charset,
                                     String signType) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);
        return verify(content, sign, publicKey, charset, signType);
    }

    /**
     * 公钥加密
     *
     * @param content   待加密内容
     * @param publicKey 公钥
     * @param charset   字符集，如UTF-8, GBK, GB2312
     * @param signType  指定采用的签名方式，RSA、RSA2、SM2
     * @return 密文内容
     * @throws RuntimeException
     */
    public static String encrypt(String content, String publicKey,
                                 String charset, String signType) throws RuntimeException {
        return AsymmetricManager.getByName(signType).encrypt(content, charset, publicKey);
    }

    /**
     * 私钥解密
     *
     * @param content    待解密内容
     * @param privateKey 私钥
     * @param charset    字符集，如UTF-8, GBK, GB2312
     * @param signType   指定采用的签名方式，RSA、RSA2、SM2
     * @return 明文内容
     * @throws RuntimeException
     */
    public static String decrypt(String content, String privateKey,
                                 String charset, String signType) throws RuntimeException {

        return AsymmetricManager.getByName(signType).decrypt(content, charset, privateKey);
    }

    /**
     * 加密并签名<br> <b>目前适用于公众号</b>
     *
     * @param bizContent      待加密、签名内容
     * @param alipayPublicKey 支付宝公钥
     * @param cusPrivateKey   商户私钥
     * @param charset         字符集，如UTF-8, GBK, GB2312
     * @param isEncrypt       是否加密，true-加密  false-不加密
     * @param isSign          是否签名，true-签名  false-不签名
     * @param signType        指定采用的签名方式，RSA、RSA2、SM2
     * @return 加密、签名后xml内容字符串 <p> 返回示例： <alipay> <response>密文</response> <encryption_type>RSA</encryption_type> <sign>sign</sign>
     * <sign_type>RSA</sign_type> </alipay> </p>
     * @throws RuntimeException
     */
    public static String encryptAndSign(String bizContent, String alipayPublicKey,
                                        String cusPrivateKey, String charset, boolean isEncrypt,
                                        boolean isSign, String signType) throws RuntimeException {
        StringBuilder sb = new StringBuilder();
        if (StringUtil.isEmpty(charset)) {
            charset = CypherConstants.CHARSET_GBK;
        }
        sb.append("<?xml version=\"1.0\" encoding=\"" + charset + "\"?>");
        if (isEncrypt) {// 加密
            sb.append("<alipay>");
            String encrypted = encrypt(bizContent, alipayPublicKey, charset, signType);
            sb.append("<response>" + encrypted + "</response>");
            sb.append("<encryption_type>RSA</encryption_type>");
            if (isSign) {
                String sign = sign(encrypted, cusPrivateKey, charset, signType);
                sb.append("<sign>" + sign + "</sign>");
                sb.append("<sign_type>RSA</sign_type>");
            }
            sb.append("</alipay>");
        } else if (isSign) {// 不加密，但需要签名
            sb.append("<alipay>");
            sb.append("<response>" + bizContent + "</response>");
            String sign = sign(bizContent, cusPrivateKey, charset, signType);
            sb.append("<sign>" + sign + "</sign>");
            sb.append("<sign_type>RSA</sign_type>");
            sb.append("</alipay>");
        } else {// 不加密，不加签
            sb.append(bizContent);
        }
        return sb.toString();
    }

    /**
     * 验签并解密 <p> <b>目前适用于公众号</b><br> params参数示例： <br>{
     * <br>biz_content=M0qGiGz+8kIpxe8aF4geWJdBn0aBTuJRQItLHo9R7o5JGhpic/MIUjvXo2BLB++BbkSq2OsJCEQFDZ0zK5AJYwvBgeRX30gvEj6eXqXRt16
     * /IkB9HzAccEqKmRHrZJ7PjQWE0KfvDAHsJqFIeMvEYk1Zei2QkwSQPlso7K0oheo/iT+HYE8aTATnkqD
     * /ByD9iNDtGg38pCa2xnnns63abKsKoV8h0DfHWgPH62urGY7Pye3r9FCOXA2Ykm8X4/Bl1bWFN/PFCEJHWe/HXj8KJKjWMO6ttsoV0xRGfeyUO8agu6t587Dl5ux5zD
     * /s8Lbg5QXygaOwo3Fz1G8EqmGhi4+soEIQb8DBYanQOS3X+m46tVqBGMw8Oe+hsyIMpsjwF4HaPKMr37zpW3fe7xOMuimbZ0wq53YP
     * /jhQv6XWodjT3mL0H5ACqcsSn727B5ztquzCPiwrqyjUHjJQQefFTzOse8snaWNQTUsQS7aLsHq0FveGpSBYORyA90qPdiTjXIkVP7mAiYiAIWW9pCEC7F3XtViKTZ8FRMM9ySicfuAlf3jtap6v2KPMtQv70X+hlmzO/IXB6W0Ep8DovkF5rB4r/BJYJLw/6AS0LZM9w5JfnAZhfGM2rKzpfNsgpOgEZS1WleG4I2hoQC0nxg9IcP0Hs+nWIPkEUcYNaiXqeBc=,
     * <br>sign=rlqgA8O+RzHBVYLyHmrbODVSANWPXf3pSrr82OCO/bm3upZiXSYrX5fZr6UBmG6BZRAydEyTIguEW6VRuAKjnaO/sOiR9BsSrOdXbD5Rhos/Xt7
     * /mGUWbTOt/F+3W0/XLuDNmuYg1yIC/6hzkg44kgtdSTsQbOC9gWM7ayB4J4c=, sign_type=RSA, <br>charset=UTF-8 <br>} </p>
     *
     * @param params
     * @param alipayPublicKey 支付宝公钥
     * @param cusPrivateKey   商户私钥
     * @param isCheckSign     是否验签
     * @param isDecrypt       是否解密
     * @param signType        指定采用的签名方式，RSA、RSA2、SM2
     * @return 解密后明文，验签失败则异常抛出
     * @throws RuntimeException
     */
    public static String checkSignAndDecrypt(Map<String, String> params, String alipayPublicKey,
                                             String cusPrivateKey, boolean isCheckSign,
                                             boolean isDecrypt, String signType) throws RuntimeException {
        String charset = params.get("charset");
        String bizContent = params.get("biz_content");
        if (isCheckSign) {
            if (!verifyV2(params, alipayPublicKey, charset, signType)) {
                throw new RuntimeException("rsaCheck failure:rsaParams=" + params);
            }
        }

        if (isDecrypt) {
            return decrypt(bizContent, cusPrivateKey, charset, signType);
        }

        return bizContent;
    }

    public static String getSignatureContent(RequestParametersHolder requestHolder) {
        return getSignContent(getSortedMap(requestHolder));
    }

    public static Map<String, String> getSortedMap(RequestParametersHolder requestHolder) {
        Map<String, String> sortedParams = new TreeMap<String, String>();
        StrngHashMap appParams = requestHolder.getApplicationParams();
        if (appParams != null && appParams.size() > 0) {
            sortedParams.putAll(appParams);
        }
        StrngHashMap protocalMustParams = requestHolder.getProtocalMustParams();
        if (protocalMustParams != null && protocalMustParams.size() > 0) {
            sortedParams.putAll(protocalMustParams);
        }
        StrngHashMap protocalOptParams = requestHolder.getProtocalOptParams();
        if (protocalOptParams != null && protocalOptParams.size() > 0) {
            sortedParams.putAll(protocalOptParams);
        }

        return sortedParams;
    }

    /**
     * @param sortedParams
     * @return
     */
    public static String getSignContent(Map<String, String> sortedParams) {
        StringBuilder content = new StringBuilder();
        List<String> keys = new ArrayList<String>(sortedParams.keySet());
        Collections.sort(keys);
        int index = 0;
        for (String key : keys) {
            String value = sortedParams.get(key);
            if (StringUtil.areNotEmpty(key, value)) {
                content.append(index == 0 ? "" : "&").append(key).append("=").append(value);
                index++;
            }
        }
        return content.toString();
    }

    public static SignSourceData extractSignContent(String str, int begin) {
        if (str == null) {
            return null;
        }

        int beginIndex = extractBeginPosition(str, begin);
        if (beginIndex >= str.length()) {
            return null;
        }

        int endIndex = extractEndPosition(str, beginIndex);
        return new SignSourceData(str.substring(beginIndex, endIndex), beginIndex, endIndex);
    }

    private static int extractBeginPosition(String responseString, int begin) {
        int beginPosition = begin;
        //找到第一个左大括号（对应响应的是JSON对象的情况：普通调用OpenAPI响应明文）
        //或者双引号（对应响应的是JSON字符串的情况：加密调用OpenAPI响应Base64串），作为待验签内容的起点
        while (beginPosition < responseString.length()
                && responseString.charAt(beginPosition) != '{'
                && responseString.charAt(beginPosition) != '"') {
            ++beginPosition;
        }
        return beginPosition;
    }

    private static int extractEndPosition(String responseString, int beginPosition) {
        //提取明文验签内容终点
        if (responseString.charAt(beginPosition) == '{') {
            return extractJsonObjectEndPosition(responseString, beginPosition);
        }
        //提取密文验签内容终点
        else {
            return extractJsonBase64ValueEndPosition(responseString, beginPosition);
        }
    }

    private static int extractJsonBase64ValueEndPosition(String responseString, int beginPosition) {
        for (int index = beginPosition; index < responseString.length(); ++index) {
            //找到第2个双引号作为终点，由于中间全部是Base64编码的密文，所以不会有干扰的特殊字符
            if (responseString.charAt(index) == '"' && index != beginPosition) {
                return index + 1;
            }
        }
        //如果没有找到第2个双引号，说明验签内容片段提取失败，直接尝试选取剩余整个响应字符串进行验签
        return responseString.length();
    }

    private static int extractJsonObjectEndPosition(String responseString, int beginPosition) {
        //记录当前尚未发现配对闭合的大括号
        LinkedList<String> braces = new LinkedList<String>();
        //记录当前字符是否在双引号中
        boolean inQuotes = false;
        //记录当前字符前面连续的转义字符个数
        int consecutiveEscapeCount = 0;
        //从待验签字符的起点开始遍历后续字符串，找出待验签字符串的终止点，终点即是与起点{配对的}
        for (int index = beginPosition; index < responseString.length(); ++index) {
            //提取当前字符
            char currentChar = responseString.charAt(index);

            //如果当前字符是"且前面有偶数个转义标记（0也是偶数）
            if (currentChar == '"' && consecutiveEscapeCount % 2 == 0) {
                //是否在引号中的状态取反
                inQuotes = !inQuotes;
            }
            //如果当前字符是{且不在引号中
            else if (currentChar == '{' && !inQuotes) {
                //将该{加入未闭合括号中
                braces.push("{");
            }
            //如果当前字符是}且不在引号中
            else if (currentChar == '}' && !inQuotes) {
                //弹出一个未闭合括号
                braces.pop();
                //如果弹出后，未闭合括号为空，说明已经找到终点
                if (braces.isEmpty()) {
                    return index + 1;
                }
            }

            //如果当前字符是转义字符
            if (currentChar == '\\') {
                //连续转义字符个数+1
                ++consecutiveEscapeCount;
            } else {
                //连续转义字符个数置0
                consecutiveEscapeCount = 0;
            }
        }

        //如果没有找到配对的闭合括号，说明验签内容片段提取失败，直接尝试选取剩余整个响应字符串进行验签
        return responseString.length();
    }

    /**
     * rsa内容签名
     *
     * @param content
     * @param privateKey
     * @param charset
     * @return
     * @throws RuntimeException
     */
    public static String rsaSign(String content, String privateKey, String charset,
                                 String signType) throws RuntimeException {

        return AsymmetricManager.getByName(signType).sign(content, charset, privateKey);
    }

    /**
     * sha256WithRsa 加签
     *
     * @param content
     * @param privateKey
     * @param charset
     * @return
     * @throws RuntimeException
     */
    public static String rsa256Sign(String content, String privateKey,
                                    String charset) throws RuntimeException {

        return AsymmetricManager.getByName("RSA2").sign(content, charset, privateKey);
    }

    /**
     * sha1WithRsa 加签
     *
     * @param content
     * @param privateKey
     * @param charset
     * @return
     * @throws RuntimeException
     */
    public static String rsaSign(String content, String privateKey,
                                 String charset) throws RuntimeException {

        return AsymmetricManager.getByName("RSA").sign(content, charset, privateKey);
    }

    public static String rsaSign(Map<String, String> params, String privateKey,
                                 String charset) throws RuntimeException {
        String signContent = getSignContent(params);

        return rsaSign(signContent, privateKey, charset);

    }

    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm,
                                                    InputStream ins) throws Exception {

        return RSAEncryptor.getPrivateKeyFromPKCS8(algorithm, ins);
    }

    public static String getSignCheckContentV1(Map<String, String> params) {
        if (params == null) {
            return null;
        }

        params.remove("sign");
        params.remove("sign_type");

        StringBuilder content = new StringBuilder();
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);

        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            content.append((i == 0 ? "" : "&") + key + "=" + value);
        }

        return content.toString();
    }

    public static String getSignCheckContentV2(Map<String, String> params) {
        if (params == null) {
            return null;
        }

        params.remove("sign");

        StringBuilder content = new StringBuilder();
        List<String> keys = new ArrayList<String>(params.keySet());
        Collections.sort(keys);

        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = params.get(key);
            content.append(i == 0 ? "" : "&").append(key).append("=").append(value);
        }

        return content.toString();
    }

    /**
     * 如果是RSA签名，请调用此方法进行验签
     *
     * @param params    待验签的从支付宝接收到的参数Map
     * @param publicKey 支付宝公钥
     * @param charset   参数内容编码集
     * @return true：验签通过；false：验签不通过
     * @throws RuntimeException
     */
    public static boolean rsaCheckV1(Map<String, String> params, String publicKey,
                                     String charset) throws RuntimeException {
        String sign = params.get("sign");
        String content = getSignCheckContentV1(params);

        return rsaCheckContent(content, sign, publicKey, charset);
    }

    public static boolean rsaCertCheckV1(Map<String, String> params, String alipayPublicCertPath,
                                         String charset) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);
        return rsaCheckV1(params, publicKey, charset);
    }

    /**
     * 如果是RSA或RSA2签名，请调用此方法进行验签
     *
     * @param params    待验签的从支付宝接收到的参数Map
     * @param publicKey 支付宝公钥
     * @param charset   参数内容编码集
     * @param signType  指定采用的签名方式，RSA或RSA2
     * @return true：验签通过；false：验签不通过
     * @throws RuntimeException
     */
    public static boolean rsaCheckV1(Map<String, String> params, String publicKey,
                                     String charset, String signType) throws RuntimeException {
        String sign = params.get("sign");
        String content = getSignCheckContentV1(params);

        return rsaCheck(content, sign, publicKey, charset, signType);
    }

    public static boolean rsaCertCheckV1(Map<String, String> params, String alipayPublicCertPath,
                                         String charset, String signType) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);
        return rsaCheckV1(params, publicKey, charset, signType);
    }

    public static boolean rsaCheckV2(Map<String, String> params, String publicKey,
                                     String charset) throws RuntimeException {
        String sign = params.get("sign");
        String content = getSignCheckContentV2(params);

        return rsaCheckContent(content, sign, publicKey, charset);
    }

    public static boolean rsaCertCheckV2(Map<String, String> params, String alipayPublicCertPath,
                                         String charset) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);
        return rsaCheckV2(params, publicKey, charset);
    }

    public static boolean rsaCheckV2(Map<String, String> params, String publicKey,
                                     String charset, String signType) throws RuntimeException {
        String sign = params.get("sign");
        String content = getSignCheckContentV2(params);

        return rsaCheck(content, sign, publicKey, charset, signType);
    }

    public static boolean rsaCertCheckV2(Map<String, String> params, String alipayPublicCertPath,
                                         String charset, String signType) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);

        return rsaCheckV2(params, publicKey, charset, signType);
    }

    public static boolean rsaCheck(String content, String sign, String publicKey, String charset,
                                   String signType) throws RuntimeException {

        return AsymmetricManager.getByName(signType).verify(content, charset, publicKey, sign);
    }

    public static boolean rsaCertCheck(String content, String sign, String alipayPublicCertPath, String charset,
                                       String signType) throws RuntimeException {
        String publicKey = getAlipayPublicKey(alipayPublicCertPath);
        return rsaCheck(content, sign, publicKey, charset, signType);
    }

    public static boolean rsa256CheckContent(String content, String sign, String publicKey,
                                             String charset) throws RuntimeException {

        return AsymmetricManager.getByName("RSA2").verify(content, charset, publicKey, sign);
    }

    public static boolean rsaCheckContent(String content, String sign, String publicKey,
                                          String charset) throws RuntimeException {

        return AsymmetricManager.getByName("RSA").verify(content, charset, publicKey, sign);
    }

    public static PublicKey getPublicKeyFromX509(String algorithm,
                                                 InputStream ins) throws Exception {

        return RSAEncryptor.getPublicKeyFromX509(algorithm, ins);
    }

    /**
     * 验签并解密 <p> <b>目前适用于公众号</b><br> params参数示例： <br>{
     * <br>biz_content=M0qGiGz+8kIpxe8aF4geWJdBn0aBTuJRQItLHo9R7o5JGhpic/MIUjvXo2BLB++BbkSq2OsJCEQFDZ0zK5AJYwvBgeRX30gvEj6eXqXRt16
     * /IkB9HzAccEqKmRHrZJ7PjQWE0KfvDAHsJqFIeMvEYk1Zei2QkwSQPlso7K0oheo/iT+HYE8aTATnkqD
     * /ByD9iNDtGg38pCa2xnnns63abKsKoV8h0DfHWgPH62urGY7Pye3r9FCOXA2Ykm8X4/Bl1bWFN/PFCEJHWe/HXj8KJKjWMO6ttsoV0xRGfeyUO8agu6t587Dl5ux5zD
     * /s8Lbg5QXygaOwo3Fz1G8EqmGhi4+soEIQb8DBYanQOS3X+m46tVqBGMw8Oe+hsyIMpsjwF4HaPKMr37zpW3fe7xOMuimbZ0wq53YP
     * /jhQv6XWodjT3mL0H5ACqcsSn727B5ztquzCPiwrqyjUHjJQQefFTzOse8snaWNQTUsQS7aLsHq0FveGpSBYORyA90qPdiTjXIkVP7mAiYiAIWW9pCEC7F3XtViKTZ8FRMM9ySicfuAlf3jtap6v2KPMtQv70X+hlmzO/IXB6W0Ep8DovkF5rB4r/BJYJLw/6AS0LZM9w5JfnAZhfGM2rKzpfNsgpOgEZS1WleG4I2hoQC0nxg9IcP0Hs+nWIPkEUcYNaiXqeBc=,
     * <br>sign=rlqgA8O+RzHBVYLyHmrbODVSANWPXf3pSrr82OCO/bm3upZiXSYrX5fZr6UBmG6BZRAydEyTIguEW6VRuAKjnaO/sOiR9BsSrOdXbD5Rhos/Xt7
     * /mGUWbTOt/F+3W0/XLuDNmuYg1yIC/6hzkg44kgtdSTsQbOC9gWM7ayB4J4c=, sign_type=RSA, <br>charset=UTF-8 <br>} </p>
     *
     * @param params
     * @param alipayPublicKey 支付宝公钥
     * @param cusPrivateKey   商户私钥
     * @param isCheckSign     是否验签
     * @param isDecrypt       是否解密
     * @return 解密后明文，验签失败则异常抛出
     * @throws RuntimeException
     */
    public static String checkSignAndDecrypt(Map<String, String> params, String alipayPublicKey,
                                             String cusPrivateKey, boolean isCheckSign,
                                             boolean isDecrypt) throws RuntimeException {
        String charset = params.get("charset");
        String bizContent = params.get("biz_content");
        if (isCheckSign) {
            if (!rsaCheckV2(params, alipayPublicKey, charset)) {
                throw new RuntimeException("rsaCheck failure:rsaParams=" + params);
            }
        }

        if (isDecrypt) {
            return rsaDecrypt(bizContent, cusPrivateKey, charset);
        }

        return bizContent;
    }

    /**
     * 加密并签名<br> <b>目前适用于公众号</b>
     *
     * @param bizContent      待加密、签名内容
     * @param alipayPublicKey 支付宝公钥
     * @param cusPrivateKey   商户私钥
     * @param charset         字符集，如UTF-8, GBK, GB2312
     * @param isEncrypt       是否加密，true-加密  false-不加密
     * @param isSign          是否签名，true-签名  false-不签名
     * @return 加密、签名后xml内容字符串 <p> 返回示例： <alipay> <response>密文</response> <encryption_type>RSA</encryption_type> <sign>sign</sign>
     * <sign_type>RSA</sign_type> </alipay> </p>
     * @throws RuntimeException
     */
    public static String encryptAndSign(String bizContent, String alipayPublicKey,
                                        String cusPrivateKey, String charset, boolean isEncrypt,
                                        boolean isSign) throws RuntimeException {
        StringBuilder sb = new StringBuilder();
        if (StringUtil.isEmpty(charset)) {
            charset = CypherConstants.CHARSET_GBK;
        }
        sb.append("<?xml version=\"1.0\" encoding=\"" + charset + "\"?>");
        if (isEncrypt) {// 加密
            sb.append("<alipay>");
            String encrypted = rsaEncrypt(bizContent, alipayPublicKey, charset);
            sb.append("<response>" + encrypted + "</response>");
            sb.append("<encryption_type>RSA</encryption_type>");
            if (isSign) {
                String sign = rsaSign(encrypted, cusPrivateKey, charset);
                sb.append("<sign>" + sign + "</sign>");
                sb.append("<sign_type>RSA</sign_type>");
            }
            sb.append("</alipay>");
        } else if (isSign) {// 不加密，但需要签名
            sb.append("<alipay>");
            sb.append("<response>" + bizContent + "</response>");
            String sign = rsaSign(bizContent, cusPrivateKey, charset);
            sb.append("<sign>" + sign + "</sign>");
            sb.append("<sign_type>RSA</sign_type>");
            sb.append("</alipay>");
        } else {// 不加密，不加签
            sb.append(bizContent);
        }
        return sb.toString();
    }

    /**
     * 公钥加密
     *
     * @param content   待加密内容
     * @param publicKey 公钥
     * @param charset   字符集，如UTF-8, GBK, GB2312
     * @return 密文内容
     * @throws RuntimeException
     */
    public static String rsaEncrypt(String content, String publicKey,
                                    String charset) throws RuntimeException {
        return AsymmetricManager.getByName("RSA").encrypt(content, charset, publicKey);
    }

    /**
     * 私钥解密
     *
     * @param content    待解密内容
     * @param privateKey 私钥
     * @param charset    字符集，如UTF-8, GBK, GB2312
     * @return 明文内容
     * @throws RuntimeException
     */
    public static String rsaDecrypt(String content, String privateKey,
                                    String charset) throws RuntimeException {

        return AsymmetricManager.getByName("RSA").decrypt(content, charset, privateKey);
    }

    /**
     * 从公钥证书中提取公钥序列号
     *
     * @param certPath 公钥证书存放路径，例如:/home/admin/cert.crt
     * @return 公钥证书序列号
     * @throws RuntimeException
     */
    public static String getCertSN(String certPath) throws RuntimeException {
        return AntCertificationUtil.getCertSN(AntCertificationUtil.getCertFromPath(certPath));
    }

    /**
     * 从公钥证书中提取公钥
     *
     * @param alipayPublicCertPath 公钥证书存放路径，例如:/home/admin/cert.crt
     * @return 公钥
     * @throws RuntimeException
     */
    public static String getAlipayPublicKey(String alipayPublicCertPath) throws RuntimeException {
        return AntCertificationUtil.getAlipayPublicKey(alipayPublicCertPath);
    }

    public static void main(String[] args) {
        String prikey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJ7I1RuSZRKl9fEySJ6oOjhSuHQPW7A2/X0l6+yhOB3ePWxBmr7PRIrIeAeAWVMrZgP1wmr/z3H6tqWIGW7mRhi8xhqL5rByZjZ+u6EaZZPTorylF6l3y1KRFk6qoaRr0ahrx+XPsV9XRjD8VpECxkISk3Po+wbKsU5sWbRPMF8hAgMBAAECgYAhdYyCnNyzi0lCJChtVmG+UKARxNK7VTshTgZi/K4IjTZAQ6rKHn8BB7FTU1Z/UhvTJcxsI6+hAwXN59oEJn4Kj2/O8EEuLdhvPoL639IDfqTF8O/oVFUyWLjQCPGviTOsuaCIxZetn6cQ1jmW+1jkNZvfYZqF/YOZPY3QyBaQSQJBAM3V3hIE7p+CoxEaDZcWtEqSprlsXEqCT2w7gc8kqAua2nI8PeRxL1huvdn2xw0/ZOiuNDgKTZ1PvsZgkvsW9H8CQQDFe3HASMeDpjJ/bDy+WfifafGOR/SG8LmgZx0N4LUjnJzdKP456ibjNVBCnwQJn6NRal2CQ+OggVttC0pfMlxfAkB+2g/q4PlCepsOXhUECS0BTFHNsldKX/aciCvewgOCa2O2xSOLlqmMAWulWW2g33HL6BgsdUUR7uRuk3PhpGr3AkADcPE8fogrfRvoKISudHbmoDv7VLmoGE3Dw8g307CUXEhHek1jyPbAgs9ROeAiIsv/wTC78l7nDxn+vnGfUwtVAkA93g4xcKklWQic/4WBcIKz788gS1GGtaKvlPS6iJKD/czTbZfCeVzYN+Bi43ytAS0/gy26Bzerba0hP6JaVOi8";
        String pubkey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCeyNUbkmUSpfXxMkieqDo4Urh0D1uwNv19JevsoTgd3j1sQZq+z0SKyHgHgFlTK2YD9cJq/89x+raliBlu5kYYvMYai+awcmY2fruhGmWT06K8pRepd8tSkRZOqqGka9Goa8flz7FfV0Yw/FaRAsZCEpNz6PsGyrFObFm0TzBfIQIDAQAB";

        Map<String, String> map = new HashMap<>();
        map.put(CypherConstants.HEADER_TIMESTAMP, "1234");
        map.put(CypherConstants.HEADER_APPID, "1");
        map.put(CypherConstants.HEADER_NONCESTR, "1234");
        map.put("data", "[{\"name\":\"yue-cloud-admin-system\",\"url\":\"/admin-system/v2/api-docs\",\"swaggerVersion\":\"2.0\",\"location\":\"/admin-system/v2/api-docs\"},{\"name\":\"yue-cloud-service-order\",\"url\":\"/service-order/v2/api-docs\",\"swaggerVersion\":\"2.0\",\"location\":\"/service-order/v2/api-docs\"}]");

        System.out.println(sign(map, prikey, CypherConstants.CHARSET_UTF8, CypherConstants.SIGN_TYPE_RSA));

        map.put(CypherConstants.HEADER_SIGN, "NqgC4Q4YcrsuvkucxlMz0w0hoNDEfsZjcTtePtpcK9CXEBuFE0Zyuaf8YT8PxMHJ7OdL0vc94OEn/H8iOm4CqQT0jCb8os1O5WO8HzZAjPWdCNV9pUg+IQYP+DXGkD2KGCyklLm7hsp7EwOBjl4KFnJgywKquYAXZ/sPRMs5mHk=");
        System.out.println(verifyV2(map, pubkey, CypherConstants.CHARSET_UTF8, CypherConstants.SIGN_TYPE_RSA));

    }
}
