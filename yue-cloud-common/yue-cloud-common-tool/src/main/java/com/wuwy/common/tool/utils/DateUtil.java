package com.wuwy.common.tool.utils;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023/4/24
 * @desc : 日期工具类
 */
public class DateUtil extends DateUtils {

    public static Date currentDate() {
        return new Date(System.currentTimeMillis());
    }
}
