package com.wuwy.common.db.common.constant;

/**
 * @author :  wuwy
 * @date :  2023/2/8
 * @desc : 数据源
 */
public class DataSourceType {

    public final static String MASTER = "master";

    public final static String SLAVE = "slave";
}
