package com.wuwy.common.db.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.wuwy.common.tool.utils.DateUtil;
import com.wuwy.common.tool.utils.IdGenerateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2022/11/18 9:34
 * @desc :
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        Date date = DateUtil.currentDate();
        // 起始版本 3.3.0(推荐使用)
        if (metaObject.getValue("id") == null) {
            this.strictInsertFill(metaObject, "id", Long.class, IdGenerateUtil.getIdLong());
        }
        this.strictInsertFill(metaObject, "createTime", Date.class, date);
        this.strictInsertFill(metaObject, "updateTime", Date.class, date);
        this.strictInsertFill(metaObject, "isDeleted", String.class, "0");
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        Date date = DateUtil.currentDate();
        // 起始版本 3.3.0(推荐)
        this.strictUpdateFill(metaObject, "updateTime", Date.class, date);
    }
}
