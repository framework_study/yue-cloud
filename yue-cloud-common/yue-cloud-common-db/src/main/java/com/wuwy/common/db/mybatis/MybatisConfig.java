package com.wuwy.common.db.mybatis;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author :  wuwy
 * @date :  2022/8/25 14:40
 * @desc :  mybatis映射文件扫描配置
 */
@Configuration
@MapperScan("com.wuwy.**.mapper.**")
public class MybatisConfig {

    /**
     * 自定义完整sql日志打印
     */
    @Bean
    public ConfigurationCustomizer mybatisConfigurationCustomizer() {
        return new ConfigurationCustomizer() {
            @Override
            public void customize(MybatisConfiguration configuration) {
                configuration.addInterceptor(new PrintSqlInterceptor());
            }
        };
    }
}
