package com.wuwy.common.object.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.common.object.entity.sys.SysDictEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc :
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDictEntity> {
    SysDictEntity selectByPrimaryKey(Long id);
}