package com.wuwy.common.object.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.common.object.entity.sys.SysDictItemEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : todo
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItemEntity> {

    int countByDictIds(@Param("dictIdList") List<Long> dictIdList);
}