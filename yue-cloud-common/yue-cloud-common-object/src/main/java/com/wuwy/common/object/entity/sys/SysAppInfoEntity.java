package com.wuwy.common.object.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统-应用信息表(SysAppInfo)实体类
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "SYS_APP_INFO")
public class SysAppInfoEntity extends BaseEntity {
    
    /**
     * 唯一主键，appid
     */
    @TableId
    private Long id;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 应用密钥，AES(appid+app_name)
     */
    private String appSecret;

    /**
     * 应用公钥
     */
    private String publicKey;

    /**
     * 应用私钥
     */
    private String privateKey;





}

