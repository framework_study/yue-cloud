package com.wuwy.common.object.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统-字典值表(SysDictItem)实体类
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "SYS_DICT_ITEM")
public class SysDictItemEntity extends BaseEntity {

    /**
     * 字典id
     */
    private Long dictId;

    /**
     * 字典值
     */
    private String dictValue;

    /**
     * 字典名称
     */
    private String dictName;





}

