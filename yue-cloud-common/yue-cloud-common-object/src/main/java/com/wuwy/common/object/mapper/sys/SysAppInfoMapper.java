package com.wuwy.common.object.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.common.object.entity.sys.SysAppInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : todo
 */
@Mapper
public interface SysAppInfoMapper extends BaseMapper<SysAppInfoEntity> {

}