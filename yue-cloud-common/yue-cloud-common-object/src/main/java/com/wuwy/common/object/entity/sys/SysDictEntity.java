package com.wuwy.common.object.entity.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统-字典表(SysDict)实体类
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "SYS_DICT")
public class SysDictEntity extends BaseEntity {

    /**
     * 字典码
     */
    private String dictCode;

    /**
     * 字典名称
     */
    private String dictName;




}

