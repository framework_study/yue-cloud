package com.wuwy.common.authserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.common.authserver.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : todo
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {
    SysUserEntity selectByPrimaryKey(Long id);
}