package com.wuwy.common.swagger.annotation;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * @author :  wuwy
 * @date :  2023/4/27
 * @desc : 自定义接口类注解，合并RestController、RequestMappering、Api注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
@RequestMapping
@Tag(name = "")
public @interface YueService {

    /**
     * 接口路径
     */
    @AliasFor(value = "path", annotation = RequestMapping.class)
    String[] mapping() default {};

    /**
     * 接口名称
     */
    @AliasFor(value = "name", annotation = Tag.class)
    String name() default "";

}
