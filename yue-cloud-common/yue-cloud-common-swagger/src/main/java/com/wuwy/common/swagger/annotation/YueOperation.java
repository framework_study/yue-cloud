package com.wuwy.common.swagger.annotation;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.*;

/**
 * @author :  wuwy
 * @date :  2023/4/27
 * @desc : 自定义接口方法注解，合并RequestMappering、ApiOperation注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
@Operation
public @interface YueOperation {

    /**
     * 接口路径
     */
    @AliasFor(value = "path", annotation = RequestMapping.class)
    String[] mapping() default {};

    /**
     * 接口名称
     */
    @AliasFor(value = "summary", annotation = Operation.class)
    String name();

    /**
     * 接口说明
     */
    @AliasFor(value = "description", annotation = Operation.class)
    String desc() default "";

    /**
     * 接口标签
     */
    @AliasFor(value = "tags", annotation = Operation.class)
    String[] tags() default "";

    /**
     * 接口方法
     */
    @AliasFor(value = "method", annotation = RequestMapping.class)
    RequestMethod[] method() default { RequestMethod.POST };

}
