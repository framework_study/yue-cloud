package com.wuwy.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(scanBasePackages = "com.wuwy.**") // scanBasePackages 包扫描，依赖注入
@EnableDiscoveryClient
@RestController
public class YueCloudGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(YueCloudGatewayApplication.class, args);
    }

}
