package com.wuwy.modlues.job.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author :  wuwy
 * @date :  2023-04-24 15:23:14
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateSysTaskDTO implements Serializable {

    private static final long serialVersionUID = 249491755737853688L;

    @Schema(description = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    
    @Schema(description = "定时任务名称")
    @Size(max = 100, message = "定时任务名称最长不超过{max}个字符")
    @NotBlank(message = "定时任务名称不能为空")
    private String taskName;
    
    @Schema(description = "定时任务类名")
    @Size(max = 200, message = "定时任务类名最长不超过{max}个字符")
    @NotBlank(message = "定时任务类名不能为空")
    private String taskClassName;
    
    @Schema(description = "cron表达式")
    @Size(max = 100, message = "cron表达式最长不超过{max}个字符")
    @NotBlank(message = "cron表达式不能为空")
    private String taskCron;
    
    @Schema(description = "说明")
    @Size(max = 500, message = "说明最长不超过{max}个字符")
    @NotBlank(message = "说明不能为空")
    private String description;



}

