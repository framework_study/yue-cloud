package com.wuwy.modlues.job.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuwy.common.swagger.annotation.YueOperation;
import com.wuwy.common.swagger.annotation.YueService;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.modlues.job.dto.CreateSysTaskDTO;
import com.wuwy.modlues.job.dto.QuerySysTaskDTO;
import com.wuwy.modlues.job.dto.ResultSysTaskDTO;
import com.wuwy.modlues.job.entity.sys.SysTaskEntity;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 系统-定时任务表(SysTask)表服务接口
 */
@YueService(mapping = "/sys/task", name = "系统-定时调度管理")
public interface SysTaskService extends IService<SysTaskEntity> {

    @YueOperation(mapping = "", name = "新增", method = RequestMethod.POST)
    void save(@RequestBody @Valid CreateSysTaskDTO dto);

    @YueOperation(mapping = "", name = "编辑", method = RequestMethod.PUT)
    void update(@RequestBody @Valid CreateSysTaskDTO dto);

    @YueOperation(mapping = "", name = "查询分页列表", method = RequestMethod.GET)
    IPage<ResultSysTaskDTO> getPage(Limit limit, QuerySysTaskDTO dto);

    @YueOperation(mapping = "/{id}", name = "查询详情", method = RequestMethod.GET)
    ResultSysTaskDTO getInfo(@PathVariable(value = "id") @RequestParam Long id);

    @YueOperation(mapping = "", name = "删除", method = RequestMethod.DELETE)
    void delete(@RequestParam("idList") List<Long> idList);

    @YueOperation(mapping = "/load", name = "加载定时任务", method = RequestMethod.GET)
    void loadJob(@RequestParam Long id);

    @YueOperation(mapping = "/resume", name = "恢复定时任务", method = RequestMethod.GET)
    void resumeJob(@RequestParam Long id);

    @YueOperation(mapping = "/stop", name = "暂停定时任务", method = RequestMethod.GET)
    void stopJob(@RequestParam Long id);

    @YueOperation(mapping = "/unload", name = "卸载定时任务", method = RequestMethod.GET)
    void unloadJob(@RequestParam Long id);

    /**
     * 加载所有定时任务
     */
    void startJob();

    @YueOperation(mapping = "/execute", name = "立即执行", method = RequestMethod.GET)
    void execute(@RequestParam Long id);
}

