package com.wuwy.modlues.job.common.config;

import com.wuwy.modlues.job.service.SysTaskService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import jakarta.annotation.Resource;

/**
 * @author :  wuwy
 * @date :  2022/11/18 9:20
 * @desc : 监听容器启动,并开始从数据库加载定时任务
 */
@Component
public class ScheduleJobInitListener implements CommandLineRunner {

    @Resource
    private SysTaskService jobService;

    @Override
    public void run(String... strings) throws Exception {
        jobService.startJob();
    }
}