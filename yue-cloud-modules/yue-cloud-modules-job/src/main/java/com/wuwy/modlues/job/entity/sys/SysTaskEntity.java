package com.wuwy.modlues.job.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统-定时任务表(SysTask)实体类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "SYS_TASK")
public class SysTaskEntity extends BaseEntity {
    private static final long serialVersionUID = 727004044385581784L;

    /**
     * 定时任务名称
     */
    private String taskName;

    /**
     * 定时任务类名
     */
    private String taskClassName;

    /**
     * cron表达式
     */
    private String taskCron;

    /**
     * 说明
     */
    private String description;

    /**
     * 01-草稿 02-待发布 03-待运行 04-运行中 05-已完成 06-执行异常
     */
    private String status;

}

