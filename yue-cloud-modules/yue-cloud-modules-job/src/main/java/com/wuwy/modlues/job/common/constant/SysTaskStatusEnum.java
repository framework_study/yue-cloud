package com.wuwy.modlues.job.common.constant;

/**
 * @author :  wuwy
 * @date :  2022/11/17 17:30
 * @desc : 01-草稿 02-待发布 03-待运行 04-运行中 05-已完成 06-执行异常
 */
public enum SysTaskStatusEnum {

    DRAFT("01", "草稿"),
    NOT_PUBLISHED("02", "待发布"),
    NOT_RUN("03", "待运行"),
    RUNNING("04", "运行中"),
    FINISHED("05", "已完成"),
    ERROR("06", "执行异常"),
    ;

    String code;
    String name;

    SysTaskStatusEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
