package com.wuwy.modlues.job.support;

/**
 * @author :  wuwy
 * @date :  2023/8/29
 * @desc :
 */
public interface YueJob {

    void execute();
}
