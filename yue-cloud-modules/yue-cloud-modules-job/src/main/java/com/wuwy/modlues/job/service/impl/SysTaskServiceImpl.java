package com.wuwy.modlues.job.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.common.tool.utils.BeanUtil;
import com.wuwy.common.tool.utils.StringUtil;
import com.wuwy.common.web.asserts.YueAssert;
import com.wuwy.common.web.exception.YueException;
import com.wuwy.modlues.job.entity.sys.SysTaskEntity;
import com.wuwy.modlues.job.mapper.sys.SysTaskMapper;
import com.wuwy.modlues.job.service.SysTaskService;
import com.wuwy.modlues.job.dto.CreateSysTaskDTO;
import com.wuwy.modlues.job.dto.QuerySysTaskDTO;
import com.wuwy.modlues.job.dto.ResultSysTaskDTO;
import com.wuwy.modlues.job.common.constant.SysTaskStatusEnum;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 系统-定时任务表(SysTask)表服务实现类
 */
@Slf4j
@Service("sysTaskServiceImpl")
public class SysTaskServiceImpl extends ServiceImpl<SysTaskMapper, SysTaskEntity> implements SysTaskService {

    @Resource
    private Scheduler scheduler;

    @Override
    public void save(@Valid CreateSysTaskDTO dto) {
        SysTaskEntity entity = BeanUtil.copy(dto, SysTaskEntity.class);
        this.save(entity);

        this.loadJob(entity.getId());
    }

    @Override
    public void update(CreateSysTaskDTO dto) {
        SysTaskEntity entity = this.getById(dto.getId());
        YueAssert.mustNotNull(entity, "定时任务不存在");

        entity = BeanUtil.copy(dto, SysTaskEntity.class);
        this.updateById(entity);

        this.loadJob(entity.getId());
    }

    @Override
    public IPage<ResultSysTaskDTO> getPage(Limit limit, QuerySysTaskDTO dto) {
        LambdaQueryWrapper<SysTaskEntity> lambdaQueryWrapper =
                new LambdaQueryWrapper<SysTaskEntity>()
                        .eq(StringUtil.isNotEmpty(dto.getStatus()), SysTaskEntity::getStatus, dto.getStatus())
                        .likeRight(StringUtil.isNotEmpty(dto.getTaskName()), SysTaskEntity::getTaskName, dto.getTaskName())
                        .orderByDesc(SysTaskEntity::getCreateTime, SysTaskEntity::getId);
        IPage<SysTaskEntity> page = this.page(limit.page(), lambdaQueryWrapper);

        return BeanUtil.copy(page, ResultSysTaskDTO.class);
    }

    @Override
    public ResultSysTaskDTO getInfo(Long id) {
        return BeanUtil.copy(this.getById(id), ResultSysTaskDTO.class);
    }

    @Override
    public void delete(List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public void loadJob(Long id) {
        LambdaQueryWrapper<SysTaskEntity> wrapper = new LambdaQueryWrapper();
        wrapper.eq(SysTaskEntity::getId, id).eq(SysTaskEntity::getStatus, SysTaskStatusEnum.NOT_PUBLISHED.getCode());
        SysTaskEntity SysTaskEntity = this.baseMapper.selectOne(wrapper);

        try {
            if (SysTaskEntity == null) {
                throw new SchedulerConfigException("未找到相关Job配置");
            }
            JobDetail jobDetail = getJobDetail(SysTaskEntity);

            CronTrigger cronTrigger = getCronTrigger(SysTaskEntity);
            scheduler.scheduleJob(jobDetail, cronTrigger);
        } catch (ClassNotFoundException e) {
            throw new YueException(e);
        } catch (SchedulerException e) {
            throw new YueException(e);
        }
    }

    @Override
    public void resumeJob(Long id) {
        try {
            scheduler.resumeJob(JobKey.jobKey(id.toString()));
        } catch (SchedulerException e) {
            throw new YueException(e);
        }
    }

    @Override
    public void stopJob(Long id) {
        try {
            scheduler.pauseJob(JobKey.jobKey(id.toString()));
        } catch (SchedulerException e) {
            throw new YueException(e);
        }
    }

    @Override
    public void unloadJob(Long id) {
        // 停止触发器
        try {
            scheduler.pauseTrigger(TriggerKey.triggerKey(id.toString()));
            // 卸载定时任务
            scheduler.unscheduleJob(TriggerKey.triggerKey(id.toString()));
            // 删除原来的job
            scheduler.deleteJob(JobKey.jobKey(id.toString()));
        } catch (SchedulerException e) {
            throw new YueException(e);
        }
    }

    /**
     * 程序启动开始加载定时任务
     */
    @Override
    public void startJob() {
        LambdaQueryWrapper<SysTaskEntity> wrapper = new LambdaQueryWrapper();
        wrapper.eq(SysTaskEntity::getStatus, SysTaskStatusEnum.NOT_RUN.getCode());
        List<SysTaskEntity> taskConfigEntities = this.baseMapper.selectList(wrapper);
        if (taskConfigEntities == null || taskConfigEntities.size() == 0) {
            log.error("定时任务加载数据为空");
            return;
        }
        try {
            for (SysTaskEntity configEntity : taskConfigEntities) {
                CronTrigger cronTrigger = null;
                JobDetail jobDetail = null;
                cronTrigger = getCronTrigger(configEntity);
                jobDetail = getJobDetail(configEntity);
                scheduler.deleteJob(JobKey.jobKey(configEntity.getId().toString()));
                scheduler.scheduleJob(jobDetail, cronTrigger);
                log.info("编号：{}定时任务加载成功", configEntity.getId());
            }
            scheduler.start();
        } catch (SchedulerException e) {
            throw new YueException(e);
        } catch (ClassNotFoundException e) {
            throw new YueException(e);
        }
    }

    // 组装JobDetail
    private JobDetail getJobDetail(SysTaskEntity configEntity) throws ClassNotFoundException {

        Class<? extends Job> aClass = Class.forName(configEntity.getTaskClassName()).asSubclass(Job.class);

        return JobBuilder.newJob()
                .withIdentity(JobKey.jobKey(configEntity.getId().toString()))
                .withDescription(configEntity.getDescription())
                .ofType(aClass).build();
    }

    // 组装CronTrigger
    private CronTrigger getCronTrigger(SysTaskEntity configEntity) {
        CronTrigger cronTrigger = null;
        CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(configEntity.getTaskCron());
        cronTrigger = TriggerBuilder.newTrigger()
                .withIdentity(TriggerKey.triggerKey(configEntity.getId().toString()))
                .withSchedule(cronScheduleBuilder)
                .build();
        return cronTrigger;
    }

    @Override
    public void execute(Long id) {
        ResultSysTaskDTO info = this.getInfo(id);
        List<String> statusList = Arrays.asList(new String[]{
                "01", "02"
        });
        if (statusList.contains(info.getStatus())) {
            throw new YueException("执行失败，请先发布该任务");
        }
        try {
            scheduler.triggerJob(JobKey.jobKey(id.toString()));
        } catch (SchedulerException e) {
            throw new YueException(e);
        }
    }

}


