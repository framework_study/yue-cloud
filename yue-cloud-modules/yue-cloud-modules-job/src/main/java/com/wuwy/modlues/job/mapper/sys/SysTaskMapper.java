package com.wuwy.modlues.job.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.modlues.job.entity.sys.SysTaskEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : todo
 */
@Mapper
public interface SysTaskMapper extends BaseMapper<SysTaskEntity> {
    SysTaskEntity selectByPrimaryKey(Long id);
}