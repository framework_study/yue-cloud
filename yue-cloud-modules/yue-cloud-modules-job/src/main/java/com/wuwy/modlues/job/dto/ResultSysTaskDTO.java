package com.wuwy.modlues.job.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author :  wuwy
 * @date :  2023-04-24 15:23:14
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultSysTaskDTO implements Serializable {
    
    private static final long serialVersionUID = -24216524120302688L;

    @Schema(description = "id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "定时任务名称")
    private String taskName;

    @Schema(description = "定时任务类名")
    private String taskClassName;

    @Schema(description = "cron表达式")
    private String taskCron;

    @Schema(description = "说明")
    private String description;

    @Schema(description = "01-草稿 02-待发布 03-待运行 04-运行中 05-已完成 06-执行异常")
    private String status;



}

