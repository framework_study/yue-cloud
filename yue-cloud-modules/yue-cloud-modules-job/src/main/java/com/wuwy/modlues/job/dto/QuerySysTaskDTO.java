package com.wuwy.modlues.job.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author :  wuwy
 * @date :  2023-04-24 15:23:14
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class QuerySysTaskDTO implements Serializable {
    
    private static final long serialVersionUID = -11850668333434859L;

    @Schema(description = "定时任务名称")
    private String taskName;

    @Schema(description = "01-草稿 02-待发布 03-待运行 04-运行中 05-已完成 06-执行异常")
    private String status;





}

