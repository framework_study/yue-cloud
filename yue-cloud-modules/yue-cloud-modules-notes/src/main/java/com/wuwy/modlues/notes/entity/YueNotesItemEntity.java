package com.wuwy.modlues.notes.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * 备忘录-明细(YueNotesItem)实体类
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "YUE_NOTES_ITEM")
public class YueNotesItemEntity extends BaseEntity {

    /**
     * id
     */
    @TableId
    private Long id;
    
    /**
     * 备忘录id
     */
    private Integer notesId;
    
    /**
     * 备忘录明细类型
     */
    private String notesItemType;
    
    /**
     * 分钟数
     */
    private Long minutes;
    
    /**
     * 个数
     */
    private Long nums;
    
    /**
     * 组数
     */
    private Long groups;
    



}

