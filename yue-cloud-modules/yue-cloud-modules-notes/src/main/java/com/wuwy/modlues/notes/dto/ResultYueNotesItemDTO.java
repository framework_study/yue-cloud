package com.wuwy.modlues.notes.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-09-04 17:06:42
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultYueNotesItemDTO implements Serializable {
    
    private static final long serialVersionUID = 321463013178279503L;

    @Schema(description = "id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "备忘录id")
    private Integer notesId;

    @Schema(description = "备忘录明细类型")
    private String notesItemType;

    @Schema(description = "分钟数")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long minutes;

    @Schema(description = "个数")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long nums;

    @Schema(description = "组数")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long groups;

    @Schema(description = "0-否 1-是")
    private String isDeleted;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;




}

