package com.wuwy.modlues.notes.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuwy.common.swagger.annotation.YueOperation;
import com.wuwy.common.swagger.annotation.YueService;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.modlues.notes.dto.CreateYueNotesDTO;
import com.wuwy.modlues.notes.dto.QueryYueNotesDTO;
import com.wuwy.modlues.notes.dto.ResultYueNotesDTO;
import com.wuwy.modlues.notes.entity.YueNotesEntity;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author :  wuwy
 * @date :  2023-09-04 17:32:32
 * @desc : 备忘录(YueNotes)服务层
 */
@YueService(mapping = "/notes", name = "备忘录管理")
public interface YueNotesService extends IService<YueNotesEntity> {

    @YueOperation(mapping = "", name = "新增", method = RequestMethod.POST)
    void save(@RequestBody @Valid CreateYueNotesDTO dto);

    @YueOperation(mapping = "", name = "修改", method = RequestMethod.PUT)
    void update(@RequestBody @Valid CreateYueNotesDTO dto);

    @YueOperation(mapping = "", name = "查询分页列表", method = RequestMethod.GET)
    IPage<ResultYueNotesDTO> getPage(Limit limit, QueryYueNotesDTO dto);

    @YueOperation(mapping = "/{id}", name = "查询详情", method = RequestMethod.GET)
    ResultYueNotesDTO getInfo(@PathVariable(value = "id") @RequestParam Long id);

    @YueOperation(mapping = "", name = "删除", method = RequestMethod.DELETE)
    void delete(@RequestParam("idList") List<Long> idList);

}
