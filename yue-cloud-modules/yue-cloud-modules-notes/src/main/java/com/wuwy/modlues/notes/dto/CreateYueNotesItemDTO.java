package com.wuwy.modlues.notes.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-09-04 17:06:42
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateYueNotesItemDTO implements Serializable {

    private static final long serialVersionUID = -80129119907918841L;

    @Schema(description = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    @Range(message = "id大小在{min}-{max}之间", min = 0, max = -1)
    @NotNull(message = "id不能为空")
    private Long id;
    
    @Schema(description = "备忘录id")
    @Range(message = "备忘录id大小在{min}-{max}之间", min = 0, max = -1)
    @NotNull(message = "备忘录id不能为空")
    private Integer notesId;
    
    @Schema(description = "备忘录明细类型")
    @Size(max = 200, message = "备忘录明细类型最长不超过{max}个字符")
    @NotBlank(message = "备忘录明细类型不能为空")
    private String notesItemType;
    
    @Schema(description = "分钟数")
    @JsonSerialize(using = ToStringSerializer.class)
    @Range(message = "分钟数大小在{min}-{max}之间", min = 0, max = -1)
    private Long minutes;
    
    @Schema(description = "个数")
    @JsonSerialize(using = ToStringSerializer.class)
    @Range(message = "个数大小在{min}-{max}之间", min = 0, max = -1)
    private Long nums;
    
    @Schema(description = "组数")
    @JsonSerialize(using = ToStringSerializer.class)
    @Range(message = "组数大小在{min}-{max}之间", min = 0, max = -1)
    private Long groups;
    
    @Schema(description = "0-否 1-是")
    @Size(max = 3, message = "0-否 1-是最长不超过{max}个字符")
    @NotBlank(message = "0-否 1-是不能为空")
    private String isDeleted;
    
    @Schema(description = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "创建时间不能为空")
    private Date createTime;
    
    @Schema(description = "修改时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "修改时间不能为空")
    private Date updateTime;
    



}

