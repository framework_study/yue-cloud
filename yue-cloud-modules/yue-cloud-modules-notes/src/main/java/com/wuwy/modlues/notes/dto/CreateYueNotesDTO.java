package com.wuwy.modlues.notes.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-09-04 17:06:42
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateYueNotesDTO implements Serializable {

    private static final long serialVersionUID = -69454610540359564L;

    @Schema(description = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    @Range(message = "id大小在{min}-{max}之间", min = 0, max = -1)
    @NotNull(message = "id不能为空")
    private Long id;
    
    @Schema(description = "当前日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "当前日期不能为空")
    private Date currentDate;
    
    @Schema(description = "备忘录类型")
    @Size(max = 200, message = "备忘录类型最长不超过{max}个字符")
    @NotBlank(message = "备忘录类型不能为空")
    private String notesType;
    
    @Schema(description = "标题")
    @Size(max = 50, message = "标题最长不超过{max}个字符")
    @NotBlank(message = "标题不能为空")
    private String title;
    
    @Schema(description = "简介")
    @Size(max = 200, message = "简介最长不超过{max}个字符")
    @NotBlank(message = "简介不能为空")
    private String summary;
    
    @Schema(description = "0-否 1-是")
    @Size(max = 3, message = "0-否 1-是最长不超过{max}个字符")
    @NotBlank(message = "0-否 1-是不能为空")
    private String isDeleted;
    
    @Schema(description = "创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "创建时间不能为空")
    private Date createTime;
    
    @Schema(description = "修改时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(message = "修改时间不能为空")
    private Date updateTime;
    



}

