package com.wuwy.modlues.notes.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * 备忘录(YueNotes)实体类
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "YUE_NOTES")
public class YueNotesEntity extends BaseEntity {

    /**
     * id
     */
    @TableId
    private Long id;
    
    /**
     * 当前日期
     */
    private Date currentDate;
    
    /**
     * 备忘录类型
     */
    private String notesType;
    
    /**
     * 标题
     */
    private String title;
    
    /**
     * 简介
     */
    private String summary;
    



}

