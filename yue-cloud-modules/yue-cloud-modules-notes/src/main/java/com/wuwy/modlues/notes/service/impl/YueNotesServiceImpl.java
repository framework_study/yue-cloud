package com.wuwy.modlues.notes.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.common.tool.utils.BeanUtil;
import com.wuwy.modlues.notes.dto.CreateYueNotesDTO;
import com.wuwy.modlues.notes.dto.QueryYueNotesDTO;
import com.wuwy.modlues.notes.dto.ResultYueNotesDTO;
import com.wuwy.modlues.notes.entity.YueNotesEntity;
import com.wuwy.modlues.notes.mapper.YueNotesMapper;
import com.wuwy.modlues.notes.service.YueNotesService;
import jakarta.validation.Valid;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author :  wuwy
 * @date :  2023-09-05 09:53:54
 * @desc : 备忘录服务实现层
 */
@Service("yueNotesServiceImpl")
public class YueNotesServiceImpl extends ServiceImpl<YueNotesMapper, YueNotesEntity> implements YueNotesService {
    
    @Override
    public void save(@Valid CreateYueNotesDTO dto) {
        YueNotesEntity entity = BeanUtil.copy(dto, YueNotesEntity.class);
        this.save(entity);
    }

    @Override
    public void update(CreateYueNotesDTO dto) {
        YueNotesEntity entity = BeanUtil.copy(dto, YueNotesEntity.class);
        this.updateById(entity);
    }

    @Override
    public IPage<ResultYueNotesDTO> getPage(Limit limit, QueryYueNotesDTO dto) {
        LambdaQueryWrapper<YueNotesEntity> lambdaQueryWrapper =
                new LambdaQueryWrapper<YueNotesEntity>()
                        .orderByDesc(YueNotesEntity::getCreateTime, YueNotesEntity::getId);
        IPage<YueNotesEntity> page = this.page(limit.page(), lambdaQueryWrapper);

        return BeanUtil.copy(page, ResultYueNotesDTO.class);
    }

    @Override
    public ResultYueNotesDTO getInfo(Long id) {
        return BeanUtil.copy(this.getById(id), ResultYueNotesDTO.class);
    }

    @Override
    public void delete(List<Long> idList) {
        this.removeByIds(idList);
    }

}
