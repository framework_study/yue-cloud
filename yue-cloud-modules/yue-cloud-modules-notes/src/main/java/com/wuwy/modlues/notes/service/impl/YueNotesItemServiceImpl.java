package com.wuwy.modlues.notes.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.common.tool.utils.BeanUtil;
import com.wuwy.modlues.notes.dto.CreateYueNotesItemDTO;
import com.wuwy.modlues.notes.dto.QueryYueNotesItemDTO;
import com.wuwy.modlues.notes.dto.ResultYueNotesItemDTO;
import com.wuwy.modlues.notes.entity.YueNotesItemEntity;
import com.wuwy.modlues.notes.mapper.YueNotesItemMapper;
import com.wuwy.modlues.notes.service.YueNotesItemService;
import jakarta.validation.Valid;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author :  wuwy
 * @date :  2023-09-05 09:53:53
 * @desc : 备忘录-明细服务实现层
 */
@Service("yueNotesItemServiceImpl")
public class YueNotesItemServiceImpl extends ServiceImpl<YueNotesItemMapper, YueNotesItemEntity> implements YueNotesItemService {
    
    @Override
    public void save(@Valid CreateYueNotesItemDTO dto) {
        YueNotesItemEntity entity = BeanUtil.copy(dto, YueNotesItemEntity.class);
        this.save(entity);
    }

    @Override
    public void update(CreateYueNotesItemDTO dto) {
        YueNotesItemEntity entity = BeanUtil.copy(dto, YueNotesItemEntity.class);
        this.updateById(entity);
    }

    @Override
    public IPage<ResultYueNotesItemDTO> getPage(Limit limit, QueryYueNotesItemDTO dto) {
        LambdaQueryWrapper<YueNotesItemEntity> lambdaQueryWrapper =
                new LambdaQueryWrapper<YueNotesItemEntity>()
                        .orderByDesc(YueNotesItemEntity::getCreateTime, YueNotesItemEntity::getId);
        IPage<YueNotesItemEntity> page = this.page(limit.page(), lambdaQueryWrapper);

        return BeanUtil.copy(page, ResultYueNotesItemDTO.class);
    }

    @Override
    public ResultYueNotesItemDTO getInfo(Long id) {
        return BeanUtil.copy(this.getById(id), ResultYueNotesItemDTO.class);
    }

    @Override
    public void delete(List<Long> idList) {
        this.removeByIds(idList);
    }

}
