package com.wuwy.modlues.notes.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-09-04 17:06:42
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultYueNotesDTO implements Serializable {
    
    private static final long serialVersionUID = 223322876364161073L;

    @Schema(description = "id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "当前日期")
    private Date currentDate;

    @Schema(description = "备忘录类型")
    private String notesType;

    @Schema(description = "标题")
    private String title;

    @Schema(description = "简介")
    private String summary;

    @Schema(description = "0-否 1-是")
    private String isDeleted;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;




}

