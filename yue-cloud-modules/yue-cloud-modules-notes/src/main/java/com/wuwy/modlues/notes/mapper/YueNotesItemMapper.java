package com.wuwy.modlues.notes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.modlues.notes.entity.YueNotesItemEntity;

/**
 * 备忘录-明细(YueNotesItem)表数据库访问层
 * 
 */
public interface YueNotesItemMapper extends BaseMapper<YueNotesItemEntity> {

}

