package com.wuwy.modlues.notes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.modlues.notes.entity.YueNotesEntity;

/**
 * 备忘录(YueNotes)表数据库访问层
 * 
 */
public interface YueNotesMapper extends BaseMapper<YueNotesEntity> {

}

