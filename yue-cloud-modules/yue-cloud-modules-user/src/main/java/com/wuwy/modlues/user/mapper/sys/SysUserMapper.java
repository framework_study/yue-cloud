package com.wuwy.modlues.user.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wuwy.modlues.user.entity.sys.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author :  wuwy
 * @date :  2023/4/23
 * @desc : todo
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    SysUserEntity selectByUsername(@Param("username") String username);
}