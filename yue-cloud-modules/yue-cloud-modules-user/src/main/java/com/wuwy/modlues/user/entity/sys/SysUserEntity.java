package com.wuwy.modlues.user.entity.sys;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwy.common.tool.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统-用户表(SysUser)实体类
 * 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "SYS_USER")
public class SysUserEntity extends BaseEntity {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 01-可用 02-禁用
     */
    private String status;




}

