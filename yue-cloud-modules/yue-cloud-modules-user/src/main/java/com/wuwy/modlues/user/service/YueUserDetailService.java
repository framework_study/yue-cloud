package com.wuwy.modlues.user.service;

import com.wuwy.modlues.user.entity.sys.SysUserEntity;
import com.wuwy.modlues.user.mapper.sys.SysUserMapper;
import jakarta.annotation.Resource;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author :  wuwy
 * @date :  2023/8/31
 * @desc :
 */
public class YueUserDetailService implements UserDetailsService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUserEntity sysUserEntity = sysUserMapper.selectByUsername(username);
        UserDetails userDetails = User.withDefaultPasswordEncoder().build();
        return userDetails;
    }

}
