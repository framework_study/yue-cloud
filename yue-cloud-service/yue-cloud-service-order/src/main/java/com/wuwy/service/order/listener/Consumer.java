package com.wuwy.service.order.listener;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @author :  wuwy
 * @date :  2022/10/20 15:27
 * @desc :
 * 写一个类去实现RocketMQListenner<消费类型> 接口 把这个类交给spring容器来进行管理
 * 在这个类上去贴RocketMQMessageListenner,这个注解里有我们需要配置的属性
 * consumerGroup: 消费者组名  topic: 主题
 */
@Component
@RocketMQMessageListener(
        consumerGroup = "my-group",
        topic = "springboot-hello",
        messageModel = MessageModel.BROADCASTING)
public class Consumer implements RocketMQListener<String> {
    /**
     * 当topic下面有消息就会进入到onMessage方法执行消费逻辑业务
     *
     * @param
     */
    @Override
    public void onMessage(String msg) {
        System.out.println("消费 " + msg);
    }
}