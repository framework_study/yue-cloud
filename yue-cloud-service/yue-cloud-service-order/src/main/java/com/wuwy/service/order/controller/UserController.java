package com.wuwy.service.order.controller;

import com.wuwy.common.swagger.annotation.YueOperation;
import com.wuwy.common.swagger.annotation.YueService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author :  wuwy
 * @date :  2022/8/24 14:24
 * @desc :
 */
@YueService(mapping = "/order", name = "订单管理")
public class UserController {


    @YueOperation(mapping = "/name", name = "根据用户名获取用户信息", method = RequestMethod.GET)
    public String findByName(String name){
        return "model";
    }

}
