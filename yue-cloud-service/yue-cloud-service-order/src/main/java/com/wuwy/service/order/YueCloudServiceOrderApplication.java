package com.wuwy.service.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages="com.wuwy.**") // scanBasePackages 包扫描，依赖注入
@EnableDiscoveryClient
public class YueCloudServiceOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(YueCloudServiceOrderApplication.class, args);
    }

}
