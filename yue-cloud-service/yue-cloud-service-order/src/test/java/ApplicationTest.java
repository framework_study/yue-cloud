
import com.wuwy.service.order.YueCloudServiceOrderApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :  wuwy
 * @date :  2022/8/25 16:17
 * @desc :
 */
@SpringBootTest(classes = {YueCloudServiceOrderApplication.class})
@RunWith(SpringRunner.class)
@Slf4j
public class ApplicationTest {

}
