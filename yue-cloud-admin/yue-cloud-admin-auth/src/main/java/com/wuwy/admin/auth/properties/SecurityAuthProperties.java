package com.wuwy.admin.auth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :  wuwy
 * @date :  2022/8/29 9:56
 * @desc :
 */
@Data
@Component
@ConfigurationProperties(prefix = "yue.security.auth")
public class SecurityAuthProperties {
    /**
     * 不验证登录的接口
     */
    private List<String> ignoreUrls = new ArrayList<>();
    /**
     * 是否启用登录验证
     */
    private Boolean enable;
}
