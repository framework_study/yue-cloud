package com.wuwy.admin.auth.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :  wuwy
 * @date :  2022/8/26 10:03
 * @desc :
 */
@Data
@Component
@ConfigurationProperties(prefix = "yue.security.oauth2")
public class SecurityOauth2Properties {
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 客户端密钥
     */
    private String clientSecret;
    /**
     * token过期时间
     */
    private int tokenExpired;
    /**
     * refresh_token过期时间
     */
    private int refreshExpired;
    /**
     * 重定向地址
     */
    private String redirectUrl;
    /**
     * 授权类型
     */
    private List<String> grantType = new ArrayList<>();

}
