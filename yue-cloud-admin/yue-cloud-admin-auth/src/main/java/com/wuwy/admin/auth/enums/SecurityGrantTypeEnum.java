package com.wuwy.admin.auth.enums;

/**
 * @author :  wuwy
 * @date :  2022/8/29 9:17
 * @desc : oauth2授权类型
 */
public enum SecurityGrantTypeEnum {
    AUTHORIZATION_CODE("authorization_code", "授权码模式"),
    IMPLICIT("implicit", "简化模式"),
    PASSWORD("password", "密码模式模式"),
    CLIENT_CREDENTIALS("client_credentials", "客户端模式")
    ;

    SecurityGrantTypeEnum(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private String code;

    private String value;

    public String getCode() {
        return null;
    }

    public String getValue() {
        return null;
    }
}
