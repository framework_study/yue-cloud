package com.wuwy.admin.auth.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author :  wuwy
 * @date :  2022/9/20 14:58
 * @desc :
 */
@Data
public class BaseResult implements Serializable {

    private static final long serialVersionUID = 7079608124238450831L;

    /**
     * 状态码
     */
    protected int code;

    /**
     * 错误信息
     */
    protected String message;

    /**
     * 详细信息
     */
    protected String detailMessage;

    /**
     * 返回数据
     */
    protected Object data;

    protected List list;
}

