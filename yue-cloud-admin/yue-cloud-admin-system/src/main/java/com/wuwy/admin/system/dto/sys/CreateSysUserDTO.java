package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-24 15:22:56
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateSysUserDTO implements Serializable {

    private static final long serialVersionUID = 540386785596142501L;

    @Schema(description = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    
    @Schema(description = "用户名")
    @Size(max = 100, message = "用户名最长不超过{max}个字符")
    private String username;
    
    @Schema(description = "密码")
    @Size(max = 100, message = "密码最长不超过{max}个字符")
    private String password;

    



}

