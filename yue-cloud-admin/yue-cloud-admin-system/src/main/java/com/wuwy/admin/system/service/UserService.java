package com.wuwy.admin.system.service;

import com.wuwy.admin.system.dto.ResultSysUserDTO;

/**
 * @author :  wuwy
 * @date :  2022/8/25 14:46
 * @desc :
 */
public interface UserService {
    ResultSysUserDTO findByName(String name);
}
