package com.wuwy.admin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwy.admin.system.dto.sys.CreateSysDictItemDTO;
import com.wuwy.admin.system.dto.sys.QuerySysDictItemDTO;
import com.wuwy.admin.system.dto.sys.ResultSysDictItemDTO;
import com.wuwy.admin.system.service.SysDictItemService;
import com.wuwy.common.object.entity.sys.SysDictItemEntity;
import com.wuwy.common.object.mapper.sys.SysDictItemMapper;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.common.tool.utils.BeanUtil;
import com.wuwy.common.tool.utils.CollectionUtil;
import com.wuwy.common.tool.utils.StringUtil;
import com.wuwy.common.web.asserts.YueAssert;
import com.wuwy.common.web.exception.YueException;
import org.springframework.stereotype.Service;

import jakarta.validation.Valid;
import java.util.List;

/**
 * 系统-字典值表(SysDictItemItem)表服务实现类
 */

@Service("sysDictItemServiceImpl")
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItemEntity> implements SysDictItemService {

    @Override
    public void save(@Valid CreateSysDictItemDTO dto) {
        SysDictItemEntity entity = this.getByDictIdAndDictValue(dto.getDictId(), dto.getDictValue());
        YueAssert.mustNull(entity, "字典值【{0}】已存在", dto.getDictValue());

        entity = BeanUtil.copy(dto, SysDictItemEntity.class);
        this.save(entity);
    }

    @Override
    public void update(CreateSysDictItemDTO dto) {
        SysDictItemEntity entity = this.getByDictIdAndDictValue(dto.getDictId(), dto.getDictValue());
        if (entity != null && !entity.getId().equals(dto.getId())) {
            throw new YueException("字典值【{0}】已存在", dto.getDictValue());
        }

        entity = BeanUtil.copy(dto, SysDictItemEntity.class);
        this.updateById(entity);
    }

    @Override
    public IPage<ResultSysDictItemDTO> getPage(Limit limit, QuerySysDictItemDTO dto) {
        LambdaQueryWrapper<SysDictItemEntity> lambdaQueryWrapper =
                new LambdaQueryWrapper<SysDictItemEntity>()
                        .eq(StringUtil.isNotEmpty(dto.getDictId()), SysDictItemEntity::getDictId, dto.getDictId())
                        .eq(StringUtil.isNotEmpty(dto.getDictValue()), SysDictItemEntity::getDictValue, dto.getDictValue())
                        .likeRight(StringUtil.isNotEmpty(dto.getDictName()), SysDictItemEntity::getDictName, dto.getDictName())
                        .orderByDesc(SysDictItemEntity::getCreateTime, SysDictItemEntity::getId);
        IPage<SysDictItemEntity> page = this.page(limit.page(), lambdaQueryWrapper);

        return BeanUtil.copy(page, ResultSysDictItemDTO.class);
    }

    private SysDictItemEntity getByDictIdAndDictValue(Long dictId, String dictValue) {
        LambdaUpdateWrapper<SysDictItemEntity> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(SysDictItemEntity::getDictId, dictId);
        lambdaUpdateWrapper.eq(SysDictItemEntity::getDictValue, dictId);
        SysDictItemEntity entity = this.baseMapper.selectOne(lambdaUpdateWrapper);
        return entity;
    }

    @Override
    public int countByDictIds(List<Long> dictIdList) {
        if (CollectionUtil.isEmpty(dictIdList)) {
            return 0;
        }
        return this.baseMapper.countByDictIds(dictIdList);
    }

    @Override
    public ResultSysDictItemDTO getInfo(Long id) {
        return BeanUtil.copy(this.getById(id), ResultSysDictItemDTO.class);
    }

    @Override
    public void delete(List<Long> idList) {
        this.removeByIds(idList);
    }
}



