package com.wuwy.admin.system.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author :  wuwy
 * @date :  2022/8/24 17:22
 * @desc :
 */
@Data
public class ResultSysUserDTO implements Serializable {

    private static final long serialVersionUID = 6575863722380917177L;

    private String username;

    private String password;

    private String port;
}
