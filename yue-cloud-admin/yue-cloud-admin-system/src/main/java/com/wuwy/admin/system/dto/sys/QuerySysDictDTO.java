package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:11
 * @desc :
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class QuerySysDictDTO implements Serializable {
    
    private static final long serialVersionUID = -64500817389371977L;

    @Schema(description = "字典码")
    private String dictCode;

    @Schema(description = "字典名称")
    private String dictName;

}

