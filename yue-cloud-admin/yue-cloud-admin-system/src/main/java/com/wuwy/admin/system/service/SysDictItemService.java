package com.wuwy.admin.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuwy.admin.system.dto.sys.CreateSysDictItemDTO;
import com.wuwy.admin.system.dto.sys.QuerySysDictItemDTO;
import com.wuwy.admin.system.dto.sys.ResultSysDictItemDTO;
import com.wuwy.common.object.entity.sys.SysDictItemEntity;
import com.wuwy.common.swagger.annotation.YueOperation;
import com.wuwy.common.swagger.annotation.YueService;
import com.wuwy.common.tool.mybatis.Limit;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;
import java.util.List;

/**
 * 系统-字典值表(SysDictItemItem)表服务接口
 */
@YueService(mapping = "/sys/dict/item", name = "系统-字典值管理")
public interface SysDictItemService extends IService<SysDictItemEntity> {

    @YueOperation(mapping = "", name = "新增", method = RequestMethod.POST)
    void save(@RequestBody @Valid CreateSysDictItemDTO dto);

    @YueOperation(mapping = "", name = "修改", method = RequestMethod.PUT)
    void update(@RequestBody @Valid CreateSysDictItemDTO dto);

    @YueOperation(mapping = "", name = "查询分页列表", method = RequestMethod.GET)
    IPage<ResultSysDictItemDTO> getPage(Limit limit, QuerySysDictItemDTO dto);

    @YueOperation(mapping = "/{id}", name = "查询详情", method = RequestMethod.GET)
    ResultSysDictItemDTO getInfo(@PathVariable(value = "id") @RequestParam Long id);

    @YueOperation(mapping = "", name = "删除", method = RequestMethod.DELETE)
    void delete(@RequestParam("idList") List<Long> idList);

    /**
     * 查询字典id是否存在字典值
     */
    int countByDictIds(List<Long> dictIdList);
}

