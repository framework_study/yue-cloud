package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-24 15:22:56
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultSysUserDTO implements Serializable {
    
    private static final long serialVersionUID = 637700222741569952L;

    @Schema(description = "id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "盐")
    private String salt;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;

    @Schema(description = "是否删除 0-否 1-是")
    private Integer isDeleted;

    @Schema(description = "01-可用 02-禁用")
    private String status;




}

