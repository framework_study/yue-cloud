package com.wuwy.admin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwy.admin.system.dto.sys.CreateSysAppInfoDTO;
import com.wuwy.admin.system.dto.sys.ResultSysAppInfoDTO;
import com.wuwy.admin.system.service.SysAppInfoService;
import com.wuwy.common.object.entity.sys.SysAppInfoEntity;
import com.wuwy.common.object.mapper.sys.SysAppInfoMapper;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.common.tool.utils.BeanUtil;
import com.wuwy.common.tool.utils.StringUtil;
import com.wuwy.common.web.asserts.YueAssert;
import org.springframework.stereotype.Service;

import jakarta.validation.Valid;
import java.util.List;

/**
 * 系统-应用信息表(SysAppInfo)表服务实现类
 */
@Service("sysAppInfoServiceImpl")
public class SysAppInfoServiceImpl extends ServiceImpl<SysAppInfoMapper, SysAppInfoEntity> implements SysAppInfoService {

    @Override
    public void save(@Valid CreateSysAppInfoDTO dto) {
        SysAppInfoEntity entity = this.getByAppName(dto.getAppName());
        YueAssert.mustNull(entity, "应用名称【{0}】已存在", dto.getAppName());

        entity = BeanUtil.copy(dto, SysAppInfoEntity.class);
        this.save(entity);
    }

    @Override
    public void update(@Valid CreateSysAppInfoDTO dto) {
        SysAppInfoEntity entity = this.getByAppName(dto.getAppName());
        YueAssert.mustNotNull(entity, "应用名称【{0}】不存在", dto.getAppName());

        Long id = entity.getId();
        entity = new SysAppInfoEntity();
        entity.setId(id);
        entity.setAppSecret(dto.getAppSecret());
        entity.setPrivateKey(dto.getPrivateKey());
        entity.setPublicKey(dto.getPublicKey());
        this.updateById(entity);
    }

    @Override
    public IPage<ResultSysAppInfoDTO> getPage(Limit limit, String appName) {
        LambdaQueryWrapper<SysAppInfoEntity> lambdaQueryWrapper =
                new LambdaQueryWrapper<SysAppInfoEntity>()
                        .likeRight(StringUtil.isNotEmpty(appName), SysAppInfoEntity::getAppName, appName)
                        .orderByDesc(SysAppInfoEntity::getCreateTime, SysAppInfoEntity::getId);
        IPage<SysAppInfoEntity> page = this.page(limit.page(), lambdaQueryWrapper);

        return BeanUtil.copy(page, ResultSysAppInfoDTO.class);
    }

    private SysAppInfoEntity getByAppName(String appName) {
        LambdaUpdateWrapper<SysAppInfoEntity> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(SysAppInfoEntity::getAppName, appName);
        SysAppInfoEntity entity = this.baseMapper.selectOne(lambdaUpdateWrapper);
        return entity;
    }

    @Override
    public ResultSysAppInfoDTO getInfo(Long id) {
        return BeanUtil.copy(this.getById(id), ResultSysAppInfoDTO.class);
    }

    @Override
    public void delete(List<Long> idList) {
        this.removeByIds(idList);
    }
}

