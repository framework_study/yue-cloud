package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:12
 * @desc :
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class QuerySysDictItemDTO implements Serializable {
    
    private static final long serialVersionUID = 463408657155738541L;

    @Schema(description = "字典id")
    private String dictId;

    @Schema(description = "字典值")
    private String dictValue;

    @Schema(description = "字典名称")
    private String dictName;



}

