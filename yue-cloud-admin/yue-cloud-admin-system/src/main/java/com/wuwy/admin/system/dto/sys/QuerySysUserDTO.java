package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-24 15:22:56
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class QuerySysUserDTO implements Serializable {
    
    private static final long serialVersionUID = -91257640434496485L;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "01-可用 02-禁用")
    private String status;




}

