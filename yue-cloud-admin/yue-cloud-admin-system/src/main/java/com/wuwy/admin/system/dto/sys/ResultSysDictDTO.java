package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:12
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultSysDictDTO implements Serializable {
    
    private static final long serialVersionUID = 456031381228746675L;

    @Schema(description = "唯一主键 唯一主键，appid")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "字典码")
    private String dictCode;

    @Schema(description = "字典名称")
    private String dictName;

    @Schema(description = "0-否 1-是")
    private Integer isDeleted;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;




}

