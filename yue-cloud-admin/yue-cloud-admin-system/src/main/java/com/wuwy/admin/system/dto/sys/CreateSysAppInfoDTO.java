package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:11
 * @desc : 应用信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateSysAppInfoDTO implements Serializable {

    private static final long serialVersionUID = -73722607888252944L;

    @Schema(description = "应用名称")
    @Size(max = 100, message = "应用名称最长不超过{max}个字符")
    @NotBlank(message = "应用名称不能为空")
    private String appName;
    
    @Schema(description = "应用密钥，AES(appid+app_name)")
    @Size(max = 100, message = "应用密钥，AES(appid+app_name)最长不超过{max}个字符")
    @NotBlank(message = "应用密钥，AES(appid+app_name)不能为空")
    private String appSecret;
    
    @Schema(description = "应用公钥")
    @Size(max = 1000, message = "应用公钥最长不超过{max}个字符")
    @NotBlank(message = "应用公钥不能为空")
    private String publicKey;
    
    @Schema(description = "应用私钥")
    @Size(max = 1000, message = "应用私钥最长不超过{max}个字符")
    @NotBlank(message = "应用私钥不能为空")
    private String privateKey;



}

