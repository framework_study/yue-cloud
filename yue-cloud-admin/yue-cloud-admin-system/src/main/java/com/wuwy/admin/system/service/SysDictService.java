package com.wuwy.admin.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuwy.admin.system.dto.sys.CreateSysDictDTO;
import com.wuwy.admin.system.dto.sys.QuerySysDictDTO;
import com.wuwy.admin.system.dto.sys.ResultSysDictDTO;
import com.wuwy.common.object.entity.sys.SysDictEntity;
import com.wuwy.common.swagger.annotation.YueOperation;
import com.wuwy.common.swagger.annotation.YueService;
import com.wuwy.common.tool.mybatis.Limit;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;
import java.util.List;

/**
 * 系统-字典表(SysDict)表服务接口
 */
@YueService(mapping = "/sys/dict", name = "系统-字典管理")
public interface SysDictService extends IService<SysDictEntity> {

    @YueOperation(mapping = "", name = "新增", method = RequestMethod.POST)
    void save(@RequestBody @Valid CreateSysDictDTO dto);

    @YueOperation(mapping = "", name = "修改", method = RequestMethod.PUT)
    void update(@RequestBody @Valid CreateSysDictDTO dto);

    @YueOperation(mapping = "", name = "查询分页列表", method = RequestMethod.GET)
    IPage<ResultSysDictDTO> getPage(Limit limit, QuerySysDictDTO dto);

    @YueOperation(mapping = "/{id}", name = "查询详情", method = RequestMethod.GET)
    ResultSysDictDTO getInfo(@PathVariable(value = "id") @RequestParam Long id);

    @YueOperation(mapping = "", name = "删除", method = RequestMethod.DELETE)
    void delete(@RequestParam("idList") List<Long> idList);
}

