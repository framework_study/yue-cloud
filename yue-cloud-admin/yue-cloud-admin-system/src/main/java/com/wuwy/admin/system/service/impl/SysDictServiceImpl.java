package com.wuwy.admin.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wuwy.admin.system.dto.sys.CreateSysDictDTO;
import com.wuwy.admin.system.dto.sys.QuerySysDictDTO;
import com.wuwy.admin.system.dto.sys.ResultSysDictDTO;
import com.wuwy.admin.system.service.SysDictItemService;
import com.wuwy.admin.system.service.SysDictService;
import com.wuwy.common.object.entity.sys.SysDictEntity;
import com.wuwy.common.object.mapper.sys.SysDictMapper;
import com.wuwy.common.tool.mybatis.Limit;
import com.wuwy.common.tool.utils.BeanUtil;
import com.wuwy.common.tool.utils.StringUtil;
import com.wuwy.common.web.asserts.YueAssert;
import com.wuwy.common.web.exception.YueException;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import java.util.List;

/**
 * 系统-字典表(SysDict)表服务实现类
 */
@Service("sysDictServiceImpl")
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDictEntity> implements SysDictService {

    @Resource
    private SysDictItemService sysDictItemService;

    @Override
    public void save(@Valid CreateSysDictDTO dto) {
        SysDictEntity entity = this.getByDictCode(dto.getDictCode());
        YueAssert.mustNull(entity, "字典码【{0}】已存在", dto.getDictCode());

        entity = BeanUtil.copy(dto, SysDictEntity.class);
        this.save(entity);
    }

    @Override
    public void update(CreateSysDictDTO dto) {
        SysDictEntity entity = this.getByDictCode(dto.getDictCode());
        if (entity != null && !entity.getId().equals(dto.getId())) {
            throw new YueException("字典码【{0}】已存在", dto.getDictCode());
        }

        entity = BeanUtil.copy(dto, SysDictEntity.class);
        this.updateById(entity);
    }

    @Override
    public IPage<ResultSysDictDTO> getPage(Limit limit, QuerySysDictDTO dto) {
        LambdaQueryWrapper<SysDictEntity> lambdaQueryWrapper =
                new LambdaQueryWrapper<SysDictEntity>()
                        .eq(StringUtil.isNotEmpty(dto.getDictCode()), SysDictEntity::getDictCode, dto.getDictCode())
                        .likeRight(StringUtil.isNotEmpty(dto.getDictName()), SysDictEntity::getDictName, dto.getDictName())
                        .orderByDesc(SysDictEntity::getCreateTime, SysDictEntity::getId);
        IPage<SysDictEntity> page = this.page(limit.page(), lambdaQueryWrapper);

        return BeanUtil.copy(page, ResultSysDictDTO.class);
    }

    @Override
    public ResultSysDictDTO getInfo(Long id) {
        return BeanUtil.copy(this.getById(id), ResultSysDictDTO.class);
    }

    @Override
    public void delete(List<Long> idList) {
        // 判断是否存在字典明细
        int i = sysDictItemService.countByDictIds(idList);
        if (i > 0) {
            throw new YueException("请先删除对应字典值");
        }
        this.removeByIds(idList);
    }

    private SysDictEntity getByDictCode(String dictCode) {
        LambdaUpdateWrapper<SysDictEntity> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(SysDictEntity::getDictCode, dictCode);
        SysDictEntity entity = this.baseMapper.selectOne(lambdaUpdateWrapper);
        return entity;
    }
}

