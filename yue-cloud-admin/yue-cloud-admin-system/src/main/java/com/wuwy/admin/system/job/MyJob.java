package com.wuwy.admin.system.job;

import com.wuwy.modlues.job.support.YueJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author :  wuwy
 * @date :  2022/11/18 11:02
 * @desc :
 */
@Slf4j
@Component
public class MyJob implements YueJob {

    @Override
    public void execute() {
        log.info("MyJob开始执行=============");
    }
}
