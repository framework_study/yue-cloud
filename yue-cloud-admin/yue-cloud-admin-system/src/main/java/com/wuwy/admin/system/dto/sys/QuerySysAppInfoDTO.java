package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:11
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class QuerySysAppInfoDTO implements Serializable {
    
    private static final long serialVersionUID = -36216054712027942L;

    @Schema(description = "唯一主键，appid")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "应用名称")
    private String appName;

    @Schema(description = "应用密钥，AES(appid+app_name)")
    private String appSecret;

    @Schema(description = "应用公钥")
    private String publicKey;

    @Schema(description = "应用私钥")
    private String privateKey;

    @Schema(description = "是否删除 0-否 1-是")
    private Integer isDeleted;

    @Schema(description = "创建时间")
    private Date createTime;

    @Schema(description = "修改时间")
    private Date updateTime;




}

