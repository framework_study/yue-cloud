package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:12
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultSysDictItemDTO implements Serializable {
    
    private static final long serialVersionUID = -44517098068488871L;

    @Schema(description = "字典值id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @Schema(description = "字典id")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long dictId;

    @Schema(description = "字典值")
    private String dictValue;

    @Schema(description = "字典名称")
    private String dictName;




}

