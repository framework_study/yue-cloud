package com.wuwy.admin.system.dto.sys;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:11
 * @desc : 应用信息
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ResultSysAppInfoDTO implements Serializable {
    
    private static final long serialVersionUID = 388357282170640510L;

    @Schema(description = "应用名称")
    private String appName;

    @Schema(description = "应用密钥，AES(appid+app_name)")
    private String appSecret;

    @Schema(description = "应用公钥")
    private String publicKey;

    @Schema(description = "应用私钥")
    private String privateKey;

    @Schema(description = "创建时间")
    private Date createTime;




}

