package com.wuwy.admin.system.dto.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author :  wuwy
 * @date :  2023-04-23 17:01:12
 * @desc : todo
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CreateSysDictItemDTO implements Serializable {

    private static final long serialVersionUID = 207601561591415885L;

    @Schema(description = "字典值id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    
    @Schema(description = "字典id")
    @NotNull(message = "字典id不能为空")
    private Long dictId;
    
    @Schema(description = "字典值")
    @Size(max = 100, message = "字典值最长不超过{max}个字符")
    @NotBlank(message = "字典值不能为空")
    private String dictValue;
    
    @Schema(description = "字典名称")
    @Size(max = 100, message = "字典名称最长不超过{max}个字符")
    @NotBlank(message = "字典名称不能为空")
    private String dictName;


}

