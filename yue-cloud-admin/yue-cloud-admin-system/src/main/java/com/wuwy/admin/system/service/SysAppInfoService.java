package com.wuwy.admin.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wuwy.admin.system.dto.sys.CreateSysAppInfoDTO;
import com.wuwy.admin.system.dto.sys.ResultSysAppInfoDTO;
import com.wuwy.common.object.entity.sys.SysAppInfoEntity;
import com.wuwy.common.swagger.annotation.YueOperation;
import com.wuwy.common.swagger.annotation.YueService;
import com.wuwy.common.tool.mybatis.Limit;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.validation.Valid;
import java.util.List;

/**
 * 系统-应用信息表(SysAppInfo)表服务接口
 */
@YueService(mapping = "/sys/appinfo", name = "系统-应用信息管理")
public interface SysAppInfoService extends IService<SysAppInfoEntity> {

    @YueOperation(mapping = "", name = "新增", method = RequestMethod.POST)
    void save(@RequestBody @Valid CreateSysAppInfoDTO dto);

    @YueOperation(mapping = "", name = "编辑", method = RequestMethod.PUT)
    void update(@RequestBody @Valid CreateSysAppInfoDTO dto);

    @YueOperation(mapping = "", name = "查询分页列表", method = RequestMethod.GET)
    IPage<ResultSysAppInfoDTO> getPage(
            Limit limit,
            @Parameter(name = "appName", description = "应用名称") @RequestParam String appName
    );

    @YueOperation(mapping = "/{id}", name = "查询详情", method = RequestMethod.GET)
    ResultSysAppInfoDTO getInfo(@PathVariable(value = "id") Long id);

    @YueOperation(mapping = "", name = "删除", method = RequestMethod.DELETE)
    void delete(@RequestParam("idList") List<Long> idList);
}

